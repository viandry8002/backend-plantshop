# HTML [Widget Box Dashboard]
![Widget Box Dashboard](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/raw/master/docs/img/widget-box-dashboard.jpg)

path file ```\app\View\Components\Dashboard\Box.php```

```bash
public function render()
{
  return view('components.dashboard.box', [
    'boxs' => [
      boxDashboard(['bg-info', Modal::getCount(), 'Slider', 'fas fa-folder', url('sliders')]),
    ]
  ]);
}
```
Penjelasan :
- ```bg-info``` sebagai html class background color
- ```Modal::getCount()``` total data
- ```Slider``` untuk judul dibawah total data
- ```fas fa-folder``` icon widget box dashboard
- ```url('sliders')``` merujuk untuk link tombol selengkapnya