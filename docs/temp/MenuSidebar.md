# Menu Sidebar?
Langkah-langkah menambahkan menu backend di sidebar:
1. ketikan sql insert data pada table `features`
>![Menu Sidebar](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/raw/master/docs/img/sidebar/sql.png)

2. tambahkan data `features` yang sudah dibuat pada otoritas user di halaman backend `Otoritas Fitur` edit atau tambah otoritas sesuai kebutuhan
>[http://127.0.0.1:8000/authority](http://127.0.0.1:8000/authority)
![Otoritas Fitur](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/raw/master/docs/img/sidebar/otoritas.png)

3. edit file `\resources\views\components\sidebar\div.blade.php` untuk menambahkan tampilan menu di sidebar
```bash
<x-sidebar.menu url="{{url('test')}}" key="test" icon="fas fa-lock" :authority="$authority" />
```
_Penjelasan : untuk `key` diharuskan sama dengan data sql insert field `code`_

# Submenu sidebar?
Langkah-langkah menambahkan submenu backend di sidebar:
1. ketikan sql insert data pada table `features`
>![Submenu sidebar](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/raw/master/docs/img/sidebar/sql-sub.png)
2. Lakukan langkah kedua seperti membuat __Menu Sidebar__
3. edit file `\resources\views\components\sidebar\div.blade.php` untuk menambahkan tampilan menu di sidebar
```bash
<li class="nav-item{{(in_array(Route::currentRouteName(), $master_data)) ? ' menu-open' : ''}}">
  <a href="#" class="nav-link{{(in_array(Route::currentRouteName(), $master_data)) ? ' active' : ''}}">
    <i class="nav-icon fas fa-folder"></i>
    <p>
      Master Data
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    # tambahkan menu lainnya jika ingin menambahkan sub menu di master data
    <x-sidebar.menu url="{{url('sliders')}}" key="slider" :authority="$authority" />
    <x-sidebar.menu url="{{url('test')}}" key="test" :authority="$authority" />
  </ul>
</li>
```
edit juga file `\app\View\Components\Sidebar\Div.php`
```bash
public function render()
{
  return view('components.sidebar.div',[
    'master_data' => ['slider', 'test'],
    'authority' => Authority::find(auth()->user()->authorities_id),
  ]);
}
```
_Penjelasan : untuk `key` diharuskan sama dengan data sql insert field `code`_

# Langkah terakhir untuk Menu / Submenu
edit file `\routes\web.php`
```bash
Route::prefix('test')->group(function () {
  Route::get('/', [TestController::class, 'index'])->name('test');
  Route::get('/add', [TestController::class, 'add'])->name('test');
  Route::get('/edit/{id}', [TestController::Class, 'edit'])->name('test');
  Route::post('/_create', [TestController::class, 'create'])->name('test');
  Route::post('/_edit', [TestController::Class, 'update'])->name('test');
  Route::post('/_delete', [TestController::class, 'delete'])->name('test');
});
```
_Penjelasan : untuk `name=('test')` diharuskan sama dengan data sql insert field `code`_
