# CKEditor ?
Cara penggunaan CKEditor v.5
```bash
<textarea name="desc" class="form-control ckeditor editor"></textarea>

<script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
<script type="text/javascript" src="{{asset('js/ckeditor5.js')}}"></script>
<script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
```

Jika ingin menggunakan ckeditor lebih dari 1 
```bash
<textarea name="desc" class="form-control ckeditor editor"></textarea>
<textarea name="desc" class="form-control ckeditor editor2"></textarea>

<script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
<script type="text/javascript" src="{{asset('js/ckeditor5.js')}}"></script>
<script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
```

Penjelasan : dari mana class ```editor2``` didapat?

> Bisa dilihat file ```\public\js\ckeditor5.js```

Tools pada CKEditor
```bash
var ext_toolbar = [ 
  'heading', '|', 'bold', 'italic', 'link', 'numberedList', 'bulletedList', '|', 'outdent', 'indent', '|', 'undo', 'redo',
]
```

Initialize CKEditor class baru
```bash
ClassicEditor
.create( document.querySelector( '.[masukan nama class baru]' ), {
  toolbar: ext_toolbar
} )
.then( editor => {
  window.editor = editor;
} )
.catch( err => {
  console.error( err.stack );
} );
```