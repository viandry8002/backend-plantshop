# HTML [Rows Category]
![Rows Category](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/raw/master/docs/img/category.png)
```bash
<div class="col-md-6" id="data-{{$item->id}}">
  <div class="info-box">
    <span class="info-box-icon elevation-1">
      <a href="{{asset($item->file)}}" target="_blank" rel="noopener noreferrer">
        <img src="{{asset($item->file)}}" class="img-fluid">
      </a>
    </span>
    <div class="info-box-content">
      <span class="info-box-text">
        {{$item->type == 'shipment' ? 'Pengiriman' : 'Pembayaran'}}
        &bull;
        <a href="{{$item->url}}" target="_blank" rel="noopener noreferrer">{{$item->url}}</a>
      </span>
      <span class="info-box-number mt-0">
        <a href="{{url('path/edit/'.$item->id)}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Ubah</a>
        <form style="display: inline-block" action="{{url('path/_delete')}}" data-id="{{$item->id}}" class="deleteForm mt-1">
          <button type="submit" class="btn btn-danger btn-sm">
            <i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus
          </button>
        </form>
      </span>
    </div>
  </div>
</div>
```