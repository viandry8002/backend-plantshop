const element = document.currentScript.getAttribute('element');

$(`.deleteForm`).submit(function(e){
  var id = $(this).data('id')
  swalWaiting()
  $.ajax({
    url: this.action,
    type: 'POST',
    data:{id},
    success: function (e) {
      Swal.close()
      if (e.success) {
        if(element === 'table'){
          $(`#action-${id}`).html('<span class="badge bg-danger">Data berhasil di hapus! <br />Refresh halaman untuk data terbaru</span>')
          $(`#data-${id}`).css('background', '#dee2e6')
        } else {
          $(`#data-${id}`).remove()
        }
      } else {
        Swal.fire({
          title: 'Gagal!',
          text: e.message,
          icon: 'error',
        });
      }
    },
    error: function(xhr, status, error) {
      Swal.close()
      var err = eval("(" + xhr.responseText + ")");
      Swal.fire({
        title: 'Gagal!',
        text: err.message,
        icon: 'error',
        buttons: {
          cancel: "Tutup",
        },
      });
    }
  });
  return false
})