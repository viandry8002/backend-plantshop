var $editors = $(".ckeditor");
if ($editors.length) {
  for (instance in ClassicEditor.instances) 
  {
    ClassicEditor.instances[instance].updateElement();
  }
} 
  
$('._form').submit(function(e){
  swalWaiting()
  
  $.ajax({
    url : this.action,
    type : "POST",
    data : new FormData(this),
    processData : false,
    contentType : false,
    cache : false,
    success: function (e) {
      console.log(e);
      Swal.close()
      Swal.fire({
        title: (e.success == true) ? 'Berhasil!' : 'Gagal!',
        text: e.message,
        icon: (e.success == true) ? 'success' : 'error',
        buttons: {
          cancel: "Tutup",
        },
      });
      if(e.url){
        window.location.replace(e.url)
      }
    },
    error: function(xhr, status, error) {
      Swal.close()
      var err = eval("(" + xhr.responseText + ")");
      Swal.fire({
        title: 'Gagal!',
        text: err.message,
        icon: 'error',
        buttons: {
          cancel: "Tutup",
        },
      });
    }
  });
  return false;
})