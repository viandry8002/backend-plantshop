$(function () {
  var data = $('.returnDateRange').val().split("_-_");
  var startDate = new Date(data[0]);
  var endDate = new Date(data[1]);
  $('.dateRange span').html(startDate.toDateString().split(' ').slice(1).join(' ') + ' - ' + endDate.toDateString().split(' ').slice(1).join(' '));
  $('.dateRange').daterangepicker({
    ranges: {
      'Hari ini': [moment(), moment()],
      'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      '7 Hari terakhir': [moment().subtract(6, 'days'), moment()],
      '14 Hari terakhir': [moment().subtract(13, 'days'), moment()],
      '30 Hari terakhir': [moment().subtract(29, 'days'), moment()],
      'Minggu ini': [moment().startOf('week'), moment().endOf('week')],
      'Minggu lalu': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
      'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
      'Bulan lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    "locale": {
      "format": "YYYY-MM-DD",
      "separator": " - ",
      "applyLabel": "Perbarui",
      "cancelLabel": "Batalkan",
      "fromLabel": "From",
      "toLabel": "To",
      "customRangeLabel": "Sepanjang Masa",
      "weekLabel": "W",
      "daysOfWeek": [
        "Min",
        "Sen",
        "Sel",
        "Rab",
        "Kam",
        "Jum",
        "Sab"
      ],
      "monthNames": [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
      ],
      "firstDay": 1
    },
    "showDropdowns": true,
    "alwaysShowCalendars": true,
    "showCustomRangeLabel": false,
    "startDate": startDate,
    "endDate": endDate
  }, function (start, end, label) {
    $('.dateRange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
    $('.returnDateRange').val(start.format('YYYY-MM-DD') + '_-_' + end.format('YYYY-MM-DD'));
  });
});