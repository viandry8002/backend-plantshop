$("#_loginForm").validate({
  rules: {
    email: { required: true, email: true }
  },
  submitHandler: function (form) {
    $(`#_loginButton`).text('Tunggu');
    $.ajax({
      url: form.action,
      type: form.method,
      data: $(form).serialize(),
      success: function (e) {
        if (e.success) {
          window.location.href = e.url;
        }else{
          Swal.fire({
            title: (e.success == true) ? 'Berhasil!' : 'Gagal!',
            text: e.message,
            icon: (e.success == true) ? 'success' : 'error',
          });
        }
        $(`#_loginButton`).text('Masuk');
      },
      error: function(xhr, status, error) {
        var err = eval("(" + xhr.responseText + ")");
        Swal.fire({
          title: 'Gagal!',
          text: err.message,
          icon: 'error',
          buttons: {
            cancel: "Tutup",
          },
        });
        $(`#_loginButton`).text('Masuk');
      }
    });
  }
});