function timeAttr(id) {
  var from = (id === 1) ? 'start_date' : 'end_date'
  var show = (id === 2) ? 'start_date' : 'end_date'
  var attr = (id === 2) ? 'max' : 'min'
  var data = $(`#${from}`).val();
  $(`#${show}`).attr(attr, data)
}
$('#start_date').on('change', function(e){
  timeAttr(1)
})
$('#end_date').on('change', function(e){
  timeAttr(2)
})
timeAttr(1)
timeAttr(2)