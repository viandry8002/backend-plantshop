var ext_toolbar = [ 
  'heading', '|', 'bold', 'italic', 'link', 'numberedList', 'bulletedList', '|', 'outdent', 'indent', '|', 'undo', 'redo',
]

ClassicEditor
.create( document.querySelector( '.editor' ), {
  toolbar: ext_toolbar
} )
.then( editor => {
  window.editor = editor;
} )
.catch( err => {
  console.error( err.stack );
} );

ClassicEditor
.create( document.querySelector( '.editor2' ), {
  toolbar: ext_toolbar
} )
.then( editor => {
  window.editor = editor;
} )
.catch( err => {
  console.error( err.stack );
} );

ClassicEditor
.create( document.querySelector( '.editor3' ), {
  toolbar: ext_toolbar
} )
.then( editor => {
  window.editor = editor;
} )
.catch( err => {
  console.error( err.stack );
} );