# Spesification ?
- [Laravel 9](https://laravel.com/docs/9.x)
- [php 8](https://www.php.net/) 

# GUIDE
1. Installasi? [click](https://gitlab.com/muhamadaderohayat122/laravel-v.9#installasi-)
2. Menggunakan CKEditor? [click](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/blob/master/docs/temp/CKEditor.md)
3. Date input min & max validasi? [click](https://gitlab.com/muhamadaderohayat122/laravel-v.9#date-input-min-max-validasi)
4. Script js Delete data? [click](https://gitlab.com/muhamadaderohayat122/laravel-v.9#delete-data)
5. element HTML [Rows Category Page] [click](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/blob/master/docs/temp/RowsCategory.md)
5. Widget Box Dashboard? [click](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/blob/master/docs/temp/WidgetBoxDashboard.md)
6. Menu Sidebar? [click](https://gitlab.com/muhamadaderohayat122/laravel-v.9/-/blob/master/docs/temp/MenuSidebar.md)

# Installasi ?
1. ketikan ```composer i``` di terminal sesuai path project
2. lalu ketikan ```php artisan storage:link``` untuk menjalankan project
3. database ```\database\code.sql``` 
> table ```slider``` berfungsi untuk data dummy, jadi untuk installasi harap masukan sql create table ```slider```
4. lalu ketikan ```php artisan serve``` untuk menjalankan project

# Date input min & max validasi?
```bash
<input type="date" name="start" id="start_date" class="form-control" required />
<input type="date" name="end" id="end_date" class="form-control" required />

<script type="text/javascript" src="{{asset('js/date-input.js')}}"></script>
```

# Delete data?
```bash
# foreach data view table
<script type="text/javascript" src="{{asset('js/crud/delete.js')}}" element="table"></script>

# or

# foreach data view not table
<script type="text/javascript" src="{{asset('js/crud/delete.js')}}"></script>
```