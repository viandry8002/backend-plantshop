CREATE TABLE `authorities` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `code` json NOT NULL,
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL
);

CREATE TABLE `features` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `type` enum('single','master data') NOT NULL,
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL
);

CREATE TABLE `admin` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `authorities_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `active` enum('yes','no') DEFAULT 'no',
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL
);

CREATE TABLE `admin_activity` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `id_admin` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NULL,
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL
);

CREATE TABLE `slider` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT,
  `id_admin` bigint(20) NOT NULL,
  `file` text NOT NULL,
  `urutan` int(11) NOT NULL,
  `url` text NOT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL
);

CREATE INDEX authorities_id ON `admin` (`authorities_id`);

CREATE INDEX id_admin ON `admin_activity` (`id_admin`);

CREATE INDEX id_admin ON `slider` (`id_admin`);

INSERT INTO `authorities` (`id`, `title`, `code`, `created_at`, `updated_at`) VALUES ('1', 'Super Admin', '["1", "2", "3"]', '2022-06-06 09:52:19', '2022-06-06 09:52:19');

INSERT INTO `features` (`id`, `title`, `code`, `type`, `created_at`, `updated_at`) VALUES (NULL, 'Slider', 'slider', 'master data', NULL, NULL), (NULL, 'User Admin', 'users', 'single', NULL, NULL), (NULL, 'Otoritas Fitur', 'authority', 'single', NULL, NULL);

INSERT INTO `admin` (`id`, `authorities_id`, `name`, `email`, `password`, `remember_token`, `active`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Admin', 'gate@admin.com', '$2y$10$jhMJfLDcvi0YC0FoRvGxG.jDBmdpxVkMiuRC.8kP3P8siS4HCdEqe', 'cwKIMFR1ddt9QCeqQGTpaTffGuBkvVDptxENe5uSo2eHs7gTz0B6PEVJWoaP', 'yes', '2022-06-06 09:53:21', '2022-06-06 09:53:21');

ALTER TABLE `admin` ADD FOREIGN KEY (`authorities_id`) REFERENCES `authorities`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `admin_activity` ADD FOREIGN KEY (`id_admin`) REFERENCES `admin`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `slider` ADD FOREIGN KEY (`id_admin`) REFERENCES `admin`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;