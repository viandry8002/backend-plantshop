<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AuthorityController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SharingsController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\EventFileController;
use App\Http\Controllers\PlatformEcommerceController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\SharingFileController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\UserMobileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('auth.login');
})->name('login');
Route::post('/_signin', [AuthController::class, 'index'])->name('_signin');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
  Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

  Route::prefix('profile')->group(function () {
    Route::get('/', [ProfileController::class, 'profile'])->name('profile');
    Route::post('/_edit', [ProfileController::class, 'edit']);
    Route::post('/_password', [ProfileController::class, 'password']);
  });

  Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

  Route::middleware(['authority.validation'])->group(function () {
    Route::prefix('authority')->group(function () {
      Route::get('/', [AuthorityController::class, 'index'])->name('authority');
      Route::get('/add', [AuthorityController::class, 'add'])->name('authority');
      Route::post('/_create', [AuthorityController::class, 'create'])->name('authority');
      Route::post('/_delete', [AuthorityController::class, 'delete'])->name('authority');
      Route::get('/edit/{id}', [AuthorityController::class, 'edit'])->name('authority');
      Route::post('/_edit', [AuthorityController::class, 'update'])->name('authority');
    });

    Route::prefix('users')->group(function () {
      Route::get('/', [UserController::class, 'index'])->name('users');
      Route::get('/add', [UserController::class, 'add'])->name('users');
      Route::get('/edit/{id}', [UserController::class, 'edit'])->name('users');
      Route::post('/_create', [UserController::class, 'create'])->name('users');
      Route::post('/_status', [UserController::class, 'status'])->name('users');
      Route::post('/_edit', [UserController::class, 'update'])->name('users');
      Route::post('/_password', [UserController::class, 'password'])->name('users');
    });

    Route::prefix('sliders')->group(function () {
      Route::get('/', [SliderController::class, 'index'])->name('slider');
      Route::get('/add', [SliderController::class, 'add'])->name('slider');
      Route::get('/edit/{id}', [SliderController::class, 'edit'])->name('slider');
      Route::post('/_create', [SliderController::class, 'create'])->name('slider');
      Route::post('/_edit', [SliderController::class, 'update'])->name('slider');
      Route::post('/_delete', [SliderController::class, 'delete'])->name('slider');
    });

    Route::prefix('sharings')->group(function () {
      Route::get('/', [SharingsController::class, 'index'])->name('sharing');
      Route::get('/add', [SharingsController::class, 'add'])->name('sharing');
      Route::get('/edit/{id}', [SharingsController::class, 'edit'])->name('sharing');
      Route::get('/image/{id}', [SharingsController::class, 'image'])->name('sharing');
      Route::post('/_create', [SharingsController::class, 'create'])->name('sharing');
      Route::post('/_edit', [SharingsController::class, 'update'])->name('sharing');
      Route::post('/_delete', [SharingsController::class, 'delete'])->name('sharing');
    });

    Route::prefix('sharings-file')->group(function () {
      Route::get('/image/{id}', [SharingFileController::class, 'index'])->name('sharing');
      Route::post('/_create', [SharingFileController::class, 'create'])->name('sharing');
      Route::post('/_delete', [SharingFileController::class, 'delete'])->name('sharing');
    });

    Route::prefix('events')->group(function () {
      Route::get('/', [EventController::class, 'index'])->name('event');
      Route::get('/add', [EventController::class, 'add'])->name('event');
      Route::get('/edit/{id}', [EventController::class, 'edit'])->name('event');
      Route::post('/_create', [EventController::class, 'create'])->name('event');
      Route::post('/_edit', [EventController::class, 'update'])->name('event');
      Route::post('/_delete', [EventController::class, 'delete'])->name('event');
    });

    Route::prefix('events-file')->group(function () {
      Route::get('/image/{id}', [EventFileController::class, 'index'])->name('event');
      Route::post('/_create', [EventFileController::class, 'create'])->name('event');
      Route::post('/_delete', [EventFileController::class, 'delete'])->name('event');
    });

    Route::prefix('events')->group(function () {
      Route::get('/', [EventController::class, 'index'])->name('event');
      Route::get('/add', [EventController::class, 'add'])->name('event');
      Route::get('/edit/{id}', [EventController::class, 'edit'])->name('event');
      Route::post('/_create', [EventController::class, 'create'])->name('event');
      Route::post('/_edit', [EventController::class, 'update'])->name('event');
      Route::post('/_delete', [EventController::class, 'delete'])->name('event');
    });

    Route::prefix('stores')->group(function () {
      Route::get('/', [StoreController::class, 'index'])->name('store');
      Route::get('/add', [StoreController::class, 'add'])->name('store');
      Route::get('/edit/{id}', [StoreController::class, 'edit'])->name('store');
      Route::post('/_create', [StoreController::class, 'create'])->name('store');
      Route::post('/_edit', [StoreController::class, 'update'])->name('store');
      Route::post('/_delete', [StoreController::class, 'delete'])->name('store');
      Route::post('/_status', [StoreController::class, 'status'])->name('store');
      Route::post('/_password', [StoreController::class, 'password'])->name('store');
    });

    Route::prefix('productCategories')->group(function () {
      Route::get('/', [ProductCategoryController::class, 'index'])->name('product_category');
      Route::get('/add', [ProductCategoryController::class, 'add'])->name('product_category');
      Route::get('/edit/{id}', [ProductCategoryController::class, 'edit'])->name('product_category');
      Route::post('/_create', [ProductCategoryController::class, 'create'])->name('product_category');
      Route::post('/_edit', [ProductCategoryController::class, 'update'])->name('product_category');
      Route::post('/_delete', [ProductCategoryController::class, 'delete'])->name('product_category');
    });

    Route::prefix('platformEcommerce')->group(function () {
      Route::get('/', [PlatformEcommerceController::class, 'index'])->name('platform_ecommerce');
      Route::get('/add', [PlatformEcommerceController::class, 'add'])->name('platform_ecommerce');
      Route::get('/edit/{id}', [PlatformEcommerceController::class, 'edit'])->name('platform_ecommerce');
      Route::post('/_create', [PlatformEcommerceController::class, 'create'])->name('platform_ecommerce');
      Route::post('/_edit', [PlatformEcommerceController::class, 'update'])->name('platform_ecommerce');
      Route::post('/_delete', [PlatformEcommerceController::class, 'delete'])->name('platform_ecommerce');
    });

    Route::prefix('userMobile')->group(function () {
      Route::get('/', [UserMobileController::class, 'index'])->name('user_mobile');
      Route::get('/add', [UserMobileController::class, 'add'])->name('user_mobile');
      Route::get('/edit/{id}', [UserMobileController::class, 'edit'])->name('user_mobile');
      Route::post('/_create', [UserMobileController::class, 'create'])->name('user_mobile');
      Route::post('/_edit', [UserMobileController::class, 'update'])->name('user_mobile');
      Route::post('/_delete', [UserMobileController::class, 'delete'])->name('user_mobile');
    });
  });
});
