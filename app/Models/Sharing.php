<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sharing extends Model
{
    use HasFactory;
    protected $table = 'sharings';
    protected $fillable = ['id_admin', 'title', 'active', 'desc'];

    static function getData($req = "")
    {
        $data = Sharing::orderBy('id', 'desc');
        if (!empty($req['_activity'])) {
            $data->where('active', $req['active'] ?? 'no');
        }
        if (!empty($req['title'])) {
            $data->where('title', 'LIKE', '%' . $req['title'] . '%');
        }
        $result = $data->get()->map(function ($item) {
            return $item;
        });
        return $result;
    }

    static function findData($id)
    {
        $data = Sharing::find($id);
        if (!$data) {
            return $data;
        }
        return $data;
    }

    static function getCount()
    {
        return Sharing::orderBy('id', 'desc')->count();
    }
}
