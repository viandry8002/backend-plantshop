<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SharingFile extends Model
{
    use HasFactory;
    protected $table = 'sharing_file';
    protected $fillable = ['id_admin', 'id_sharing', 'file'];

    static function getData($req = "")
    {
        $data = SharingFile::orderBy('id', 'desc');
        $result = $data->get()->map(function ($item) {
            return $item;
        });
        return $result;
    }

    static function findData($id)
    {
        $data = SharingFile::find($id);
        if (!$data) {
            return $data;
        }
        return $data;
    }

    static function getCount()
    {
        return SharingFile::orderBy('id', 'desc')->count();
    }
}
