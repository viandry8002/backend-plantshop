<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventFile extends Model
{
    use HasFactory;
    protected $table = 'event_file';
    protected $fillable = ['id_admin', 'id_event', 'file'];

    static function getData($req = "")
    {
        $data = EventFile::orderBy('id', 'desc');
        $result = $data->get()->map(function ($item) {
            return $item;
        });
        return $result;
    }

    static function findData($id)
    {
        $data = EventFile::find($id);
        if (!$data) {
            return $data;
        }
        return $data;
    }

    static function getCount()
    {
        return SharingFile::orderBy('id', 'desc')->count();
    }
}
