<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
  use HasFactory;

  static function getData($type){
    $data = Feature::orderBy('title', 'asc')->where('type', $type)->get()->map(function($item) {
      return $item;
    });
    return $data;
  }
}
