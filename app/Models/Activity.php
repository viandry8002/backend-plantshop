<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
  use HasFactory;
  protected $table = 'admin_activity';
  protected $fillable = ['id_admin', 'title', 'description', 'url'];
}
