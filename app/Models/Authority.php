<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
  use HasFactory;
  protected $fillable = ['title', 'code'];
  
  static function getData(){
    $data = Authority::orderBy('id', 'desc');
    $result = $data->paginate(100)->appends(request()->except('page'));
    $result->getCollection()->transform(function ($item) {
      $features_count = json_decode($item->code, true);
      $item['features_count'] = count($features_count);
      return $item;
    });
    return $result;  
  }

  static function findData($id){
    $data = Authority::find($id);
    if(!$data){
      return $data;
    }
    $data['code'] = json_decode($data->code, true);
    return $data;
  }
}
