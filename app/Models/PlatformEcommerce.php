<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlatformEcommerce extends Model
{
    use HasFactory;
    protected $table = 'platform_ecommerce';
    protected $fillable = ['id_admin', 'title', 'file', 'active'];

    static function getData($req = "")
    {
        $data = PlatformEcommerce::orderBy('id', 'desc');
        if (!empty($req['_activity'])) {
            $data->where('active', $req['active'] ?? 'no');
        }
        if (!empty($req['title'])) {
            $data->where('title', 'LIKE', '%' . $req['title'] . '%');
        }
        $result = $data->get()->map(function ($item) {
            return $item;
        });
        return $result;
    }

    static function findData($id)
    {
        $data = PlatformEcommerce::find($id);
        if (!$data) {
            return $data;
        }
        return $data;
    }

    static function getCount()
    {
        return Event::orderBy('id', 'desc')->count();
    }
}
