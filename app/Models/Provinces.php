<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    use HasFactory;
    protected $table = 'ro_provinces';
    protected $fillable = ['province_id', 'province_name'];

    static function getData($req = "")
    {
        $data = Provinces::orderBy('province_id', 'desc');
        $result = $data->get()->map(function ($item) {
            return $item;
        });
        return $result;
    }
}
