<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    use HasFactory;
    protected $table = 'ro_cities';
    protected $fillable = ['city_id', 'province_id', 'city_name', 'postal_code'];

    static function getData($req = "")
    {
        $data = Cities::orderBy('city_id', 'asc');
        $result = $data->get()->map(function ($item) {
            return $item;
        });
        return $result;
    }
}
