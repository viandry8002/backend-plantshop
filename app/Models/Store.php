<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Store extends Model
{
    use HasApiTokens, HasFactory;
    protected $table = 'store';
    protected $fillable = ['active', 'id_province', 'id_city', 'name', 'file', 'active', 'email', 'whatsapp', 'password', 'remember_token'];
    // protected $hidden = [
    //     'remember_token',
    // ];

    static function getData($req = "")
    {
        $data = Store::orderBy('id', 'desc')->with(['province', 'city']);
        if (!empty($req['_activity'])) {
            $data->where('active', $req['active'] ?? 'no');
        }
        if (!empty($req['title'])) {
            $data->where('name', 'LIKE', '%' . $req['title'] . '%');
        }
        $result = $data->get()->map(function ($item) {
            return $item;
        });
        return $result;
    }

    static function findData($id)
    {
        $data = Store::find($id);
        if (!$data) {
            return $data;
        }
        return $data;
    }

    static function getCount()
    {
        return Store::orderBy('id', 'desc')->count();
    }

    public function province()
    {
        return $this->hasOne(Provinces::class, 'province_id', 'id_province');
    }

    public function city()
    {
        return $this->hasOne(Cities::class, 'city_id', 'id_city');
    }
}
