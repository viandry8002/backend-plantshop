<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use phpDocumentor\Reflection\DocBlock\Tags\Author;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'admin';

    protected $fillable = [
        'name',
        'email',
        'password',
        'authorities_id',
        'active'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    static function findData()
    {
        $data = User::find(auth()->user()->id);
        if (!$data) {
            return $data;
        }
        $authority = Authority::find($data->authorities_id);
        $codes = json_decode($authority->code, true);
        foreach ($codes as $code) {
            $access[] = Feature::find($code)->code;
        }
        $authority['code'] = $access;
        $data['authority'] = $authority;
        return $data;
    }

    static function getData($req = "")
    {
        $data = User::with('authority')->orderBy('id', 'desc')->whereNotIn('id', [auth()->user()->id]);
        if (empty($req['_activity'])) {
            $data->where('active', 'yes');
        } else {
            $data->where('active', (!empty($req['active'])) ? 'yes' : 'no');
        }
        if (!empty($req['id'])) {
            $data->where('id', $req['id']);
        }
        if (!empty($req['authorities_id'])) {
            $data->where('authorities_id', $req['authorities_id']);
        }
        if (!empty($req['name'])) {
            $data->where('name', 'LIKE', '%' . $req['name'] . '%');
        }
        $result = $data->paginate(100)->appends(request()->except('page'));
        $result->getCollection()->transform(function ($item) {
            return $item;
        });
        return $result;
    }

    function authority()
    {
        return $this->belongsTo(Authority::class, 'authorities_id');
    }
}
