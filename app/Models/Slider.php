<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
  use HasFactory;
  protected $table = 'slider';
  protected $fillable = ['id_admin', 'file', 'urutan', 'related', 'id_related', 'active'];

  static function getData($req = "")
  {
    $data = Slider::orderBy('id', 'desc');
    if (!empty($req['_activity'])) {
      $data->where('active', $req['active'] ?? 'no');
    }
    if (!empty($req['url'])) {
      $data->where('url', 'LIKE', '%' . $req['url'] . '%');
    }
    $result = $data->get()->map(function ($item) {
      return $item;
    });
    return $result;
  }

  static function findData($id)
  {
    $data = Slider::find($id);
    if (!$data) {
      return $data;
    }
    return $data;
  }

  static function getCount()
  {
    return Slider::orderBy('id', 'desc')->count();
  }
}
