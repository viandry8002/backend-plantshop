<?php
function boxDashboard($data){
  return [
    'class' => $data[0],
    'count' => $data[1],
    'title' => $data[2],
    'icon' => $data[3],
    'url' => $data[4],
  ];
}

function bulan($data)
{
  $bulan = date('m', strtotime($data));
  if ($bulan == '01') {
    return 'Januari';
  } elseif ($bulan == '02') {
    return 'Februari';
  } elseif ($bulan == '03') {
    return 'Maret';
  } elseif ($bulan == '04') {
    return 'April';
  } elseif ($bulan == '05') {
    return 'Mei';
  } elseif ($bulan == '06') {
    return 'Juni';
  } elseif ($bulan == '07') {
    return 'Juli';
  } elseif ($bulan == '08') {
    return 'Agustus';
  } elseif ($bulan == '09') {
    return 'September';
  } elseif ($bulan == '10') {
    return 'Oktober';
  } elseif ($bulan == '11') {
    return 'November';
  } elseif ($bulan == '12') {
    return 'Desember';
  }
}

function tgl($data)
{
  return  bulan($data) . ' ' . date('d', strtotime($data)) .', '. date('Y', strtotime($data));
}

function search_tgl(){
  return date('Y-m-d', strtotime('- 3 years')).'_-_'.date('Y-m-d');
}

function opacity($data){
  if ($data == 'yes') {
    return null;
  }
  return ' style="opacity: .3"';
}

function active($data){
  if ($data == 'yes') {
    return '<span class="btn btn-sm btn-warning"><i class="fas fa-eye"></i></span>';
  }
  return '<span class="btn btn-sm btn-danger"><i class="fas fa-eye-slash"></i></span>';
}

function no($key, $limit = ""){
  return (!empty($_GET['page'])) ? ((int) $_GET['page'] * (int) $limit ?? 100) + $key + 1 : $key + 1;

}