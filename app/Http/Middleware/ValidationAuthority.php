<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class ValidationAuthority
{
    public function handle(Request $request, Closure $next)
    {
        $get = User::findData()->toArray();
        $currPath = Route::currentRouteName();
        if(in_array($currPath, $get['authority']['code'])){
            return $next($request);
        }
        return redirect('dashboard');
    }
}
