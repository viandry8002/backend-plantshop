<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventFileController extends Controller
{
    function index($id)
    {
        $ids = Event::findData($id);
        return view('event.image', [
            'title' => 'event',
            'item' => $ids,
            'collection' => $ids === null ? '' : EventFile::where('id_event', $id)->get()
        ]);
    }

    function create(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'file' => 'required|image|max:2228'
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $create = EventFile::create([
                'file' => $this->upload($req->file, 'event/' . $req->id_event),
                'id_admin' => auth()->user()->id,
                'id_event' => $req->id_event,
            ]);
            $this->activity([
                'title' => 'Tambah Event File',
                'description' => 'Telah menambahkan data EventFile #' . $create->id,
                'url' => 'eventFiles?id=' . $create->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('events-file/image', $req->id_event)
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function delete(Request $req)
    {
        try {
            $data = EventFile::find($req->id);
            $this->deleteFile($data->file);
            $this->activity([
                'title' => 'Hapus event File',
                'description' => 'Telah menghapus data event #' . $data->id,
                'url' => null,
            ]);
            $data->delete();
            return [
                'code' => 200,
                'success' => true,
            ];
        } catch (\Throwable $err) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $err->getMessage(),
            ];
        }
    }
}
