<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
  function index(Request $req)
  {
    try {
      $get = User::where('email', $req->email)->first();
      if (!$get || !Hash::check($req->password, $get->password)) {
        return [
          'code' => 404,
          'success' => false,
          'message' => 'Ups, email atau password yang anda masukan salah'
        ];
      }
      if ($get->active != 'yes') {
        return [
          'code' => 500,
          'success' => false,
          'message' => 'Ups, akun anda di nonaktifkan'
        ];
      }
      Auth::guard('web')->login($get, $remember = true);
      return [
        'code' => 200,
        'success' => true,
        'url' => route('dashboard')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function logout(Request $request)
  {
    Auth::guard('web')->logout();
    $request->session()->invalidate();
    $request->session()->regenerateToken();
    return redirect('/');
  }
}
