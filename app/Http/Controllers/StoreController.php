<?php

namespace App\Http\Controllers;

use App\Models\Cities;
use App\Models\Provinces;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    function index(Request $req)
    {
        return view('Store.index', [
            'title' => 'Store',
            'collection' => Store::getData($req->all())
        ]);
    }

    function add()
    {
        return view('Store.add', [
            'title' => 'Store',
            'provinces' => Provinces::getData(),
            'cities' => Cities::getData(),
        ]);
    }

    function edit($id)
    {
        return view('Store.edit', [
            'title' => 'Store',
            'provinces' => Provinces::getData(),
            'cities' => Cities::getData(),
            'item' => Store::findData($id),
            'collection' => Store::getData()
        ]);
    }

    function status(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'id' => 'required'
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $find = Store::find($req->id);
            $update = $find->update([
                'active' => $find->active == 'yes' ? 'no' : 'yes'
            ]);
            return [
                'code' => 201,
                'success' => true,
                'active' => $find->active
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function create(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'id_province' => 'required',
                'id_city' => 'required',
                'name' => 'required',
                'email' => 'required',
                'whatsapp' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $create = Store::create([
                'file' => 'store/upload/store/store.png',
                'id_province' => $req->id_province,
                'id_city' => $req->id_city,
                'name' => $req->name,
                'active' => 'yes',
                'email' => $req->email,
                'whatsapp' => $req->whatsapp,
                'password' => Hash::make($req->password),
                'remember_token' => ''
            ]);
            $this->activity([
                'title' => 'Tambah Store',
                'description' => 'Telah menambahkan data Store #' . $create->id,
                'url' => 'stores?id=' . $create->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('stores')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function update(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'id_province' => 'required',
                'id_city' => 'required',
                'name' => 'required',
                'email' => 'required',
                'whatsapp' => 'required'
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $find = Store::find($req->id);
            if (!$find) {
                return [
                    'code' => 404,
                    'success' => false,
                    'message' => 'Data tidak ditemukan'
                ];
            }
            $find->update($req->only(['id_province', 'id_city', 'name', 'email', 'whatsapp']));
            return [
                'code' => 201,
                'success' => true,
                'message' => 'Profile berhasil diubah.'
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function password(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'password' => 'required|confirmed|min:8|max:30',
                'id' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $data = Store::find($req->id);
            $data->update([
                'password' => Hash::make($req->password)
            ]);
            return [
                'code' => 200,
                'success' => true,
                'message' => 'Password berhasil diubah.'
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }
}
