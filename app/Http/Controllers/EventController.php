<?php

namespace App\Http\Controllers;

use App\Models\event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    function index(Request $req)
    {
        return view('event.index', [
            'title' => 'Event',
            'collection' => Event::getData($req->all())
        ]);
    }

    function add()
    {
        return view('event.add', [
            'title' => 'event'
        ]);
    }

    function edit($id)
    {
        return view('event.edit', [
            'title' => 'event',
            'item' => Event::findData($id),
            'collection' => Event::getData()
        ]);
    }

    function create(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'title' => 'required',
                'desc' => 'required',
                'start' => 'required',
                'end' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $create = Event::create([
                'id_admin' => auth()->user()->id,
                'title' => $req->title,
                'start' => $req->start,
                'end' => $req->end,
                'active' => 'yes',
                'desc' => $req->desc
            ]);
            $this->activity([
                'title' => 'Tambah event',
                'description' => 'Telah menambahkan data event #' . $create->id,
                'url' => 'events?id=' . $create->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('events')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function update(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'title' => 'required',
                'desc' => 'required',
                'start' => 'required',
                'end' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $data = Event::findData($req->id);
            if (!$data) {
                return [
                    'code' => 404,
                    'success' => false,
                    'message' => 'Data tidak ditemukan'
                ];
            }
            $data->update([
                'title' => $req->title,
                'start' => $req->start,
                'end' => $req->end,
                'active' => ($req->active == 'yes') ? 'yes' : 'no',
                'desc' => $req->desc
            ]);
            $this->activity([
                'title' => 'Edit event',
                'description' => 'Telah mengubah data event #' . $data->id,
                'url' => 'events?id=' . $data->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('events')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function delete(Request $req)
    {
        try {
            $data = Event::find($req->id);
            // $this->deleteFile($data->file);
            $this->activity([
                'title' => 'Hapus event',
                'description' => 'Telah menghapus data event #' . $data->id,
                'url' => null,
            ]);
            $data->delete();
            return [
                'code' => 200,
                'success' => true,
            ];
        } catch (\Throwable $err) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $err->getMessage(),
            ];
        }
    }
}
