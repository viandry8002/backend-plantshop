<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    function index(Request $req)
    {
        return view('product-category.index', [
            'title' => 'Product Category',
            'collection' => ProductCategory::getData($req->all())
        ]);
    }

    function add()
    {
        return view('product-category.add', [
            'title' => 'Product Category'
        ]);
    }

    function edit($id)
    {
        return view('product-category.edit', [
            'title' => 'Product Category',
            'item' => ProductCategory::findData($id),
            'collection' => ProductCategory::getData()
        ]);
    }

    function create(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'title' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $create = ProductCategory::create([
                'id_admin' => auth()->user()->id,
                'title' => $req->title,
                'active' => 'yes',
            ]);
            $this->activity([
                'title' => 'Tambah Product Category',
                'description' => 'Telah menambahkan data Product Category #' . $create->id,
                'url' => 'productCategories?id=' . $create->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('productCategories')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function update(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'title' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $data = ProductCategory::findData($req->id);
            if (!$data) {
                return [
                    'code' => 404,
                    'success' => false,
                    'message' => 'Data tidak ditemukan'
                ];
            }
            $data->update([
                'title' => $req->title,
                'active' => ($req->active == 'yes') ? 'yes' : 'no',
            ]);
            $this->activity([
                'title' => 'Edit Product Category',
                'description' => 'Telah mengubah data Product Category #' . $data->id,
                'url' => 'productCategories?id=' . $data->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('productCategories')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function delete(Request $req)
    {
        try {
            $data = ProductCategory::find($req->id);
            // $this->deleteFile($data->file);
            $this->activity([
                'title' => 'Hapus Product Category',
                'description' => 'Telah menghapus data Product Category #' . $data->id,
                'url' => null,
            ]);
            $data->delete();
            return [
                'code' => 200,
                'success' => true,
            ];
        } catch (\Throwable $err) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $err->getMessage(),
            ];
        }
    }
}
