<?php

namespace App\Http\Controllers;

use App\Models\Authority;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthorityController extends Controller
{
  function index(){
    return view('authority.index');
  }

  function add(){
    return view('authority.add');
  }

  function edit($id){
    $find = Authority::findData($id);
    return view('authority.edit', $find);
  }

  function create(Request $req){
    try {
      $validator = Validator::make($req->all(), [
        'title' => 'required',
        'code' => 'required'
      ], [
        'code.required' => 'Wajib memilih fitur setidaknya 1'
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $post = [
        'title' => $req->title,
        'code' => json_encode($req->code),
      ];
      Authority::create($post);
      return [
        'code' => 201,
        'success' => true,
        'url' => url('authority')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function delete(Request $req){
    try {
      $data = Authority::find($req->id);
      $data->delete();
      return [
        'code' => 200,
        'success' => true,
      ];
    } catch (\Throwable $err) {
      $message = ($err->errorInfo[1] === 1451) ? 'Otoritas sudah terdaftar di user admin lain' : $err->getMessage();
      return [
        'code' => 500,
        'success' => false,
        'message' => $message,
      ];
    }
  }

  function update(Request $req){
    try {
      $validator = Validator::make($req->all(), [
        'id' => 'required',
        'title' => 'required',
        'code' => 'required'
      ], [
        'code.required' => 'Wajib memilih fitur setidaknya 1'
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $find = Authority::find($req->id);
      if (!$find) {
        return [
          'code' => 404,
          'success' => false,
          'message' => 'Data tidak ditemukan!'
        ];
      }
      $post = [
        'title' => $req->title,
        'code' => json_encode($req->code),
      ];
      $find->update($post);
      return [
        'code' => 201,
        'success' => true,
        'url' => url('authority')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }
}
