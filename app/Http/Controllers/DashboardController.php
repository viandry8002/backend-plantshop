<?php

namespace App\Http\Controllers;

use App\Models\User;

class DashboardController extends Controller
{
  function index(){
    return view('dashboard');
  }
}
