<?php

namespace App\Http\Controllers;

use App\Models\Sharing;
use App\Models\SharingFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SharingFileController extends Controller
{
    function index($id)
    {
        $ids = Sharing::findData($id);
        return view('sharing.image', [
            'title' => 'sharing',
            'item' => $ids,
            'collection' => $ids === null ? '' : SharingFile::where('id_sharing', $id)->get()
        ]);
    }

    function create(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'file' => 'required|image|max:2228'
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $create = SharingFile::create([
                'file' => $this->upload($req->file, 'sharing/' . $req->id_sharing),
                'id_admin' => auth()->user()->id,
                'id_sharing' => $req->id_sharing,
            ]);
            $this->activity([
                'title' => 'Tambah Sharing File',
                'description' => 'Telah menambahkan data sharingFile #' . $create->id,
                'url' => 'sharingFiles?id=' . $create->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('sharings-file/image', $req->id_sharing)
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function delete(Request $req)
    {
        try {
            $data = SharingFile::find($req->id);
            $this->deleteFile($data->file);
            $this->activity([
                'title' => 'Hapus Sharing File',
                'description' => 'Telah menghapus data sharing #' . $data->id,
                'url' => null,
            ]);
            $data->delete();
            return [
                'code' => 200,
                'success' => true,
            ];
        } catch (\Throwable $err) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $err->getMessage(),
            ];
        }
    }
}
