<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
  function profile(){
    return view('profile.edit');
  }

  function edit(Request $req){
    try {
      $validator = Validator::make($req->all(), [
        'email' => 'required|unique:admin,email,'.auth()->user()->id,
        'name' => 'required',
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      User::find(auth()->user()->id)->update($req->only(['name', 'email']));
      return [
        'code' => 200,
        'success' => true,
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function password(Request $req){
    try {
      $validator = Validator::make($req->all(), [
        'password' => 'required|confirmed|min:8|max:30',
        'curr_password' => 'required',
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $data = User::find(auth()->user()->id);
      if (!Hash::check($req->curr_password, $data->password)) {
        return [
          'code' => 422,
          'success' => false,
          'message' => 'Password sekarang yang anda masukan salah.'
        ];
      }
      $data->update([
        'password' => Hash::make($req->password)
      ]);
      return [
        'code' => 200,
        'success' => true,
        'message' => 'Password berhasil diubah.'
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }
}
