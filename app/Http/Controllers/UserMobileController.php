<?php

namespace App\Http\Controllers;

use App\Models\UserMobile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserMobileController extends Controller
{
    function index(Request $req)
    {
        return view('user-mobile.index', [
            'title' => 'User Mobile',
            'collection' => UserMobile::getData($req->all())
        ]);
    }

    function add()
    {
        return view('user-mobile.add', [
            'title' => 'User Mobile'
        ]);
    }

    function edit($id)
    {
        return view('user-mobile.edit', [
            'title' => 'User Mobile',
            'item' => UserMobile::findData($id),
            'collection' => UserMobile::getData()
        ]);
    }

    function create(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $create = UserMobile::create([
                'name' => $req->name,
                'email' => $req->email,
                'password' => $req->password,
                'active' => 'yes',
                'file' => 'store/upload/store/store.png',
            ]);
            $this->activity([
                'title' => 'Tambah User Mobile',
                'description' => 'Telah menambahkan data User Mobile #' . $create->id,
                'url' => 'userMobile?id=' . $create->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('userMobile')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function update(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $data = UserMobile::findData($req->id);
            if (!$data) {
                return [
                    'code' => 404,
                    'success' => false,
                    'message' => 'Data tidak ditemukan'
                ];
            }
            $data->update([
                'name' => $req->name,
                'email' => $req->email,
                'password' => $req->password,
                'active' => ($req->active == 'yes') ? 'yes' : 'no'
            ]);
            $this->activity([
                'title' => 'Edit User Mobile',
                'description' => 'Telah mengubah data User Mobile #' . $data->id,
                'url' => 'userMobile?id=' . $data->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('userMobile')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function delete(Request $req)
    {
        try {
            $data = UserMobile::find($req->id);
            // $this->deleteFile($data->file);
            $this->activity([
                'title' => 'Hapus User Mobile',
                'description' => 'Telah menghapus data User Mobile #' . $data->id,
                'url' => null,
            ]);
            $data->delete();
            return [
                'code' => 200,
                'success' => true,
            ];
        } catch (\Throwable $err) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $err->getMessage(),
            ];
        }
    }
}
