<?php

namespace App\Http\Controllers;

use App\Models\PlatformEcommerce;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PlatformEcommerceController extends Controller
{
    function index(Request $req)
    {
        return view('platform-ecommerce.index', [
            'title' => 'Platform Ecommerce',
            'collection' => PlatformEcommerce::getData($req->all())
        ]);
    }

    function add()
    {
        return view('platform-ecommerce.add', [
            'title' => 'Platform Ecommerce'
        ]);
    }

    function edit($id)
    {
        return view('platform-ecommerce.edit', [
            'title' => 'Platform Ecommerce',
            'item' => PlatformEcommerce::findData($id),
            'collection' => PlatformEcommerce::getData()
        ]);
    }

    function create(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'title' => 'required',
                'file' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $create = PlatformEcommerce::create([
                'id_admin' => auth()->user()->id,
                'title' => $req->title,
                'file' => $this->upload($req->file, 'event/' . $req->id_event),
                'active' => 'yes',
            ]);
            $this->activity([
                'title' => 'Tambah Platform Ecommerce',
                'description' => 'Telah menambahkan data Platform Ecommerce #' . $create->id,
                'url' => 'platformEcommerce?id=' . $create->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('platformEcommerce')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function update(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'title' => 'required',
                'file' => 'required',
            ]);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];
            }
            $data = PlatformEcommerce::findData($req->id);
            if (!$data) {
                return [
                    'code' => 404,
                    'success' => false,
                    'message' => 'Data tidak ditemukan'
                ];
            }
            $data->update([
                'title' => $req->title,
                'file' => ($req->file) ? $this->upload($req->file, 'slider') : $data->file,
                'active' => ($req->active == 'yes') ? 'yes' : 'no',
            ]);
            $this->activity([
                'title' => 'Edit Platform Ecommerce',
                'description' => 'Telah mengubah data Platform Ecommerce #' . $data->id,
                'url' => 'platformEcommerce?id=' . $data->id,
            ]);
            return [
                'code' => 200,
                'success' => true,
                'url' => url('platformEcommerce')
            ];
        } catch (\Throwable $th) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    function delete(Request $req)
    {
        try {
            $data = PlatformEcommerce::find($req->id);
            // $this->deleteFile($data->file);
            $this->activity([
                'title' => 'Hapus Platform Ecommerce',
                'description' => 'Telah menghapus data Platform Ecommerce #' . $data->id,
                'url' => null,
            ]);
            $data->delete();
            return [
                'code' => 200,
                'success' => true,
            ];
        } catch (\Throwable $err) {
            return [
                'code' => 500,
                'success' => false,
                'message' => $err->getMessage(),
            ];
        }
    }
}
