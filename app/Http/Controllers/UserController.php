<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Authority;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
  function index(Request $req)
  {
    return view('users.index', [
      'title' => 'User Admin',
      'collection' => User::getData($req->all())
    ]);
  }

  function add()
  {
    return view('users.add', [
      'title' => 'User Admin',
      'authority' => Authority::orderBy('title', 'asc')->get()
    ]);
  }

  function edit($id)
  {
    $find = User::whereNotIn('id', [auth()->user()->id])->where('id', $id)->first();
    if (!$find) {
      return redirect(url('users'));
    }
    return view('users.edit', [
      'title' => 'User Admin',
      'authority' => Authority::orderBy('title', 'asc')->get(),
      'find' => $find
    ]);
  }

  function status(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'id' => 'required'
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $find = User::find($req->id);
      $update = $find->update([
        'active' => $find->active == 'yes' ? 'no' : 'yes'
      ]);
      return [
        'code' => 201,
        'success' => true,
        'active' => $find->active
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function create(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'authorities_id' => 'required',
        'name' => 'required',
        'email' => 'required|unique:admin,email|email',
        'password' => 'required',
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $post = array_merge($req->only(['authorities_id', 'name', 'email']), [
        'password' => Hash::make($req->password),
        'active' => 1
      ]);
      User::create($post);
      return [
        'code' => 201,
        'success' => true,
        'url' => url('users')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function update(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'authorities_id' => 'required',
        'name' => 'required',
        'id' => 'required|numeric',
        'email' => 'required|email|unique:admin,email,' . $req->id,
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $find = User::find($req->id);
      if (!$find) {
        return [
          'code' => 404,
          'success' => false,
          'message' => 'Data tidak ditemukan'
        ];
      }
      $find->update($req->only(['authorities_id', 'name', 'email']));
      return [
        'code' => 201,
        'success' => true,
        'message' => 'Profile berhasil diubah.'
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function password(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'password' => 'required|confirmed|min:8|max:30',
        'id' => 'required|numeric',
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $data = User::find($req->id);
      $data->update([
        'password' => Hash::make($req->password)
      ]);
      return [
        'code' => 200,
        'success' => true,
        'message' => 'Password berhasil diubah.'
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }
}
