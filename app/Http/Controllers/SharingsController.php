<?php

namespace App\Http\Controllers;

use App\Models\Sharing;
use App\Models\SharingFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SharingsController extends Controller
{
  function index(Request $req)
  {
    return view('sharing.index', [
      'title' => 'sharing',
      'collection' => Sharing::getData($req->all())
    ]);
  }

  function add()
  {
    return view('sharing.add', [
      'title' => 'sharing'
    ]);
  }

  function edit($id)
  {
    return view('sharing.edit', [
      'title' => 'sharing',
      'item' => Sharing::findData($id),
      'collection' => Sharing::getData()
    ]);
  }

  function create(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'title' => 'required',
        'desc' => 'required',
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $create = Sharing::create([
        'id_admin' => auth()->user()->id,
        'title' => $req->title,
        'active' => 'yes',
        'desc' => $req->desc
      ]);
      $this->activity([
        'title' => 'Tambah sharing',
        'description' => 'Telah menambahkan data sharing #' . $create->id,
        'url' => 'sharings?id=' . $create->id,
      ]);
      return [
        'code' => 200,
        'success' => true,
        'url' => url('sharings')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function update(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'title' => 'required',
        'desc' => 'required',
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $data = Sharing::findData($req->id);
      if (!$data) {
        return [
          'code' => 404,
          'success' => false,
          'message' => 'Data tidak ditemukan'
        ];
      }
      $data->update(
        [
          'active' => ($req->active == 'yes') ? 'yes' : 'no',
          'title' => $req->title,
          'desc' => $req->desc
        ]
      );
      $this->activity([
        'title' => 'Edit sharing',
        'description' => 'Telah mengubah data sharing #' . $data->id,
        'url' => 'sharings?id=' . $data->id,
      ]);
      return [
        'code' => 200,
        'success' => true,
        'url' => url('sharings')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function delete(Request $req)
  {
    try {
      $data = Sharing::find($req->id);
      // $this->deleteFile($data->file);
      $this->activity([
        'title' => 'Hapus sharing',
        'description' => 'Telah menghapus data sharing #' . $data->id,
        'url' => null,
      ]);
      $data->delete();
      return [
        'code' => 200,
        'success' => true,
      ];
    } catch (\Throwable $err) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $err->getMessage(),
      ];
    }
  }
}
