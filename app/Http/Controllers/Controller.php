<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use InvalidArgumentException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function upload($img, $folder)
  {
    try {
      $folder_ext = explode('/', $folder);
      $extension = $img->getClientOriginalExtension();
      $file = rand(000000, 999999) . '_' . $folder_ext[0] . '.' . $extension;
      $path = $img->storeAs('public/upload/' . $folder, $file);
      return 'storage/upload/' . $folder . '/' . $file;
    } catch (\Throwable $th) {
      throw new InvalidArgumentException($th->getMessage(), 500);
    }
  }

  function deleteFile($img)
  {
    if (file_exists($img)) {
      unlink($img);
    }
  }

  function activity($req)
  {
    return Activity::create(array_merge([
      'id_admin' => auth()->user()->id
    ], $req));
  }
}
