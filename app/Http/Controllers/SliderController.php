<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
  function index(Request $req)
  {
    return view('slider.index', [
      'title' => 'Slider',
      'collection' => Slider::getData($req->all())
    ]);
  }

  function add()
  {
    return view('slider.add', [
      'title' => 'Slider'
    ]);
  }

  function edit($id)
  {
    return view('slider.edit', [
      'title' => 'Slider',
      'item' => Slider::findData($id),
      'collection' => Slider::getData()
    ]);
  }

  function create(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'file' => 'required|image|max:2228'
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $get = Slider::orderBy('urutan', 'desc')->first();
      $urutan = ($get) ? $get->urutan + 1 : 1;
      $create = Slider::create([
        'file' => $this->upload($req->file, 'slider'),
        'id_admin' => auth()->user()->id,
        'related' => 'sharing',
        'id_related' => 0,
        'urutan' => $urutan,
        'active' => 'no'
      ]);
      $this->activity([
        'title' => 'Tambah Slider',
        'description' => 'Telah menambahkan data slider #' . $create->id,
        'url' => 'sliders?id=' . $create->id,
      ]);
      return [
        'code' => 200,
        'success' => true,
        'url' => url('sliders')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function update(Request $req)
  {
    try {
      $validator = Validator::make($req->all(), [
        'id' => 'required',
        'file' => 'image|max:2228'
      ]);
      if ($validator->fails()) {
        return [
          'code' => 422,
          'success' => false,
          'message' => $validator->errors()->first()
        ];
      }
      $data = Slider::findData($req->id);
      if (!$data) {
        return [
          'code' => 404,
          'success' => false,
          'message' => 'Data tidak ditemukan'
        ];
      }
      if ($req->file) {
        $this->deleteFile($data->file);
      }
      if ($req->id != $req->urutan) {
        $find = Slider::findData($req->urutan);
        $find_change = ['urutan' => $data->urutan];
        $urutan = $find->urutan;
      } else {
        $urutan = $data->urutan;
      }
      $data->update(array_merge($req->only('url'), [
        'file' => ($req->file) ? $this->upload($req->file, 'slider') : $data->file,
        'active' => ($req->active == 'yes') ? 'yes' : 'no',
        'urutan' => $urutan
      ]));
      if ($req->id != $req->urutan) {
        $find->update($find_change);
        $this->activity([
          'title' => 'Edit Slider',
          'description' => 'Telah mengubah data slider #' . $find->id,
          'url' => 'sliders?id=' . $find->id,
        ]);
      }
      $this->activity([
        'title' => 'Edit Slider',
        'description' => 'Telah mengubah data slider #' . $data->id,
        'url' => 'sliders?id=' . $data->id,
      ]);
      return [
        'code' => 200,
        'success' => true,
        'url' => url('sliders')
      ];
    } catch (\Throwable $th) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $th->getMessage()
      ];
    }
  }

  function delete(Request $req)
  {
    try {
      $data = Slider::find($req->id);
      $this->deleteFile($data->file);
      $this->activity([
        'title' => 'Hapus Slider',
        'description' => 'Telah menghapus data slider #' . $data->id,
        'url' => null,
      ]);
      $data->delete();
      return [
        'code' => 200,
        'success' => true,
      ];
    } catch (\Throwable $err) {
      return [
        'code' => 500,
        'success' => false,
        'message' => $err->getMessage(),
      ];
    }
  }
}
