<?php

namespace App\View\Components\Search;

use App\Models\Authority;
use Illuminate\View\Component;

class Users extends Component
{

  public function __construct()
  {
    //
  }

  public function render()
  {
    return view('components.search.users', [
      'authority' => Authority::orderBy('title', 'asc')->get()
    ]);
  }
}
