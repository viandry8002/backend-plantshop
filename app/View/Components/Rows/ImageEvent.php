<?php

namespace App\View\Components\Rows;

use Illuminate\View\Component;

class ImageEvent extends Component
{
    public $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function render()
    {
        return view('components.rows.imageEvent');
    }
}
