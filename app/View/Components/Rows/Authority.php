<?php

namespace App\View\Components\Rows;

use App\Models\Authority as M_Authority;
use Illuminate\View\Component;

class Authority extends Component
{

  public function __construct()
  {
    //
  }

  public function render()
  {
    return view('components.rows.authority', [
      'items' => M_Authority::getData()
    ]);
  }
}
