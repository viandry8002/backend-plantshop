<?php

namespace App\View\Components\Rows;

use App\Models\Feature as M_Feature;
use Illuminate\View\Component;

class Feature extends Component
{
  public $checked;
  public $type;
  public $title;

  public function __construct($type, $title, $checked = "")
  {
    $this->type = $type;
    $this->title = $title;
    $this->checked = $checked;
  }

  public function render()
  {
    return view('components.rows.feature', [
      'items' => M_Feature::getData($this->type)
    ]);
  }
}
