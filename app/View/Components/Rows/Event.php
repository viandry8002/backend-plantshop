<?php

namespace App\View\Components\Rows;

use Illuminate\View\Component;

class Event extends Component
{
    public $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function render()
    {
        return view('components.rows.event');
    }
}
