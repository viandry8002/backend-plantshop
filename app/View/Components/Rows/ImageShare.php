<?php

namespace App\View\Components\Rows;

use Illuminate\View\Component;

class ImageShare extends Component
{
    public $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function render()
    {
        return view('components.rows.imageShare');
    }
}
