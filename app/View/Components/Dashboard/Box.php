<?php

namespace App\View\Components\Dashboard;

use App\Models\Slider;
use Illuminate\View\Component;

class Box extends Component
{
  public function __construct()
  {
    //
  }

  public function render()
  {
    return view('components.dashboard.box', [
      'boxs' => [
        boxDashboard(['bg-info', Slider::getCount(), 'Slider', 'fas fa-folder', url('sliders')]),
      ]
    ]);
  }
}
