<?php

namespace App\View\Components\Sidebar;

use App\Models\User;
use App\Models\Authority;
use Illuminate\View\Component;

class Div extends Component
{

  public function __construct()
  {
    //
  }

  public function render()
  {
    return view('components.sidebar.div', [
      'master_data' => ['slider', 'sharing', 'event', 'store', 'product_category', 'platform_ecommerce', 'user_mobile'],
      'authority' => Authority::find(auth()->user()->authorities_id),
    ]);
  }
}
