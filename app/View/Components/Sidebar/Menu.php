<?php

namespace App\View\Components\Sidebar;

use App\Models\Feature;
use Illuminate\View\Component;

class Menu extends Component
{
    public $url;
    public $key;
    public $icon;
    public $authority;

    public function __construct($url, $key, $authority, $icon="")
    {
        $this->url = $url;
        $this->key = $key;
        $this->icon = $icon;
        $this->authority = $authority;
    }

    public function render()
    {
        return view('components.sidebar.menu', [
            'feature' => Feature::where('code', $this->key)->get(),
        ]);
    }
}
