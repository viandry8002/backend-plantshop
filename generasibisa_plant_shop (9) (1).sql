-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 28 Jun 2022 pada 15.09
-- Versi server: 10.2.44-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `generasibisa_plant_shop`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL,
  `authorities_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `active` enum('yes','no') DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `authorities_id`, `name`, `email`, `password`, `remember_token`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'gate@admin.com', '$2y$10$jhMJfLDcvi0YC0FoRvGxG.jDBmdpxVkMiuRC.8kP3P8siS4HCdEqe', 'cwKIMFR1ddt9QCeqQGTpaTffGuBkvVDptxENe5uSo2eHs7gTz0B6PEVJWoaP', 'yes', '2022-06-06 02:53:21', '2022-06-06 02:53:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_activity`
--

CREATE TABLE `admin_activity` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `authorities`
--

CREATE TABLE `authorities` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `code` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`code`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `authorities`
--

INSERT INTO `authorities` (`id`, `title`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '[\"1\", \"2\", \"3\"]', '2022-06-06 02:52:19', '2022-06-06 02:52:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `events`
--

CREATE TABLE `events` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `desc` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `events`
--

INSERT INTO `events` (`id`, `id_admin`, `title`, `start`, `end`, `active`, `desc`, `created_at`, `updated_at`) VALUES
(1, 1, 'Calathea Orbifolia', '2022-06-20 08:00:00', '2022-06-21 20:00:00', 'yes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Orci aliquet morbi ultricies blandit faucibus. Hac sed sociis volutpat sit donec at pellentesque. Pellentesque blandit est leo interdum sit aliquam id. At tristique amet at sit nunc, interdum. Neque libero malesuada habitasse massa massa nisi. Id elit adipiscing phasellus lacus semper ac ac congue. Vestibulum, nisl hendrerit magna tempor et, vel dapibus nisl velit. Egestas suspendisse sed massa, est eros. At adipiscing cursus eu integer. Nisi nunc pharetra, nunc sagittis dolor sit.\r\nPulvinar commodo auctor ut augue augue. Interdum dolor pulvinar suspendisse mattis sagittis lectus sagittis, sapien, feugiat. Tincidunt est mollis quam enim leo sapien. A id magna cursus dignissim nam nibh et. Lobortis at magnis dignissim egestas. Dui ante id nibh nisl, ut quisque. Vitae tellus morbi varius pretium amet. Ut magna a laoreet in nulla risus. Duis sit purus suspendisse enim.\r\nQuis eros neque ac, ut mi, cras a eget. Etiam eu lectus vestibulum vel tincidunt vitae pretium. Porta nec arcu convallis elit, aliquet maecenas sodales. Suspendisse sed amet euismod at feugiat adipiscing velit purus. Dui nulla sit venenatis sapien, ipsum risus tempor dolor. Pellentesque eget condimentum diam lectus urna faucibus tempor. Maecenas et vivamus ultricies consequat eleifend. Faucibus cras nibh.', NULL, NULL),
(2, 1, 'pameran buah', '2022-06-28 00:00:00', '2022-06-30 00:00:00', 'yes', '<p>pameran buah&nbsp;</p>', '2022-06-28 07:17:46', '2022-06-28 07:17:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `event_file`
--

CREATE TABLE `event_file` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `id_event` bigint(20) NOT NULL,
  `file` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `event_file`
--

INSERT INTO `event_file` (`id`, `id_admin`, `id_event`, `file`, `created_at`, `updated_at`) VALUES
(4, 1, 1, 'storage/upload/event/1/398307_event.jpeg', '2022-06-28 07:37:33', '2022-06-28 07:37:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `features`
--

CREATE TABLE `features` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `type` enum('single','master data') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `features`
--

INSERT INTO `features` (`id`, `title`, `code`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Slider', 'slider', 'master data', NULL, NULL),
(2, 'User Admin', 'users', 'single', NULL, NULL),
(3, 'Otoritas Fitur', 'authority', 'single', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `platform_ecommerce`
--

CREATE TABLE `platform_ecommerce` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `platform_ecommerce`
--

INSERT INTO `platform_ecommerce` (`id`, `id_admin`, `title`, `file`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tokopedia', 'storage/upload/platform_ecommerce/tokopedia.png', 'yes', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `id_store` bigint(20) NOT NULL,
  `id_product_category` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` decimal(20,0) NOT NULL,
  `price_discount` decimal(20,0) NOT NULL,
  `desc` text NOT NULL,
  `search` text NOT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `id_store`, `id_product_category`, `title`, `price`, `price_discount`, `desc`, `search`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Calathea Orbifolia', '20000', '0', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', 'flower, bunga', 'yes', '2022-06-17 07:37:16', '2022-06-17 07:37:21'),
(2, 1, 1, 'Kumis Kucing', '20000', '10000', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', 'flower, bunga, Calathea Orbifolia, Calathea, Orbifolia', 'yes', '2022-06-17 07:37:16', '2022-06-17 07:37:21'),
(3, 1, 1, 'Tanaman Cocor Bebek', '45000', '40000', '<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>', 'Tanaman Cocor Bebek', 'yes', '2022-06-27 07:36:57', '2022-06-27 07:41:11'),
(4, 1, 2, 'Bunga Mawar', '50000', '46000', '<p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur</p>', 'bunga mawar', 'yes', '2022-06-27 07:40:56', '2022-06-27 07:40:56'),
(5, 1, 1, 'Bunga Matahari', '35000', '0', '<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? [33] At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>', 'bunga matahari', 'yes', '2022-06-27 07:51:48', '2022-06-27 07:51:48'),
(6, 1, 2, 'Tanaman Monstera', '120000', '100000', '<p>Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur.</p>', 'tanaman monstera', 'yes', '2022-06-27 07:53:04', '2022-06-27 09:45:59'),
(7, 1, 1, 'Tanaman Tapak Dara', '23000', '0', '<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>', 'Tanaman Tapak Dara', 'yes', '2022-06-27 08:18:49', '2022-06-27 08:18:49'),
(8, 1, 2, 'Bunga Marigold', '75000', '70000', '<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>', 'tanaman marigold', 'yes', '2022-06-27 08:20:16', '2022-06-27 09:51:50'),
(9, 1, 1, 'Tanaman Tanduk Rusa', '24000', '0', '<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>', 'tanaman tanduk rusa', 'yes', '2022-06-27 08:21:20', '2022-06-27 08:21:20'),
(10, 1, 2, 'Tanaman Caladium', '35000', '0', '<p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse</p>', 'tanaman caladium', 'yes', '2022-06-27 08:23:14', '2022-06-27 08:23:14'),
(11, 1, 1, 'Tanaman Calathea Merak', '90000', '0', '<p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse</p>', 'tanaman calathea merak', 'yes', '2022-06-27 08:25:08', '2022-06-27 08:25:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_buy`
--

CREATE TABLE `product_buy` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `agent` text NOT NULL,
  `source` varchar(255) NOT NULL DEFAULT 'buy',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_buy`
--

INSERT INTO `product_buy` (`id`, `id_product`, `id_user`, `agent`, `source`, `created_at`, `updated_at`) VALUES
(10, 1, 1, 'PostmanRuntime/7.29.0', 'buy', '2022-06-21 10:37:32', '2022-06-21 10:37:32'),
(11, 1, 1, 'PostmanRuntime/7.29.0', 'buy', '2022-06-21 10:41:12', '2022-06-21 10:41:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_category`
--

CREATE TABLE `product_category` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_category`
--

INSERT INTO `product_category` (`id`, `id_admin`, `title`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Kategori 1', 'yes', NULL, NULL),
(2, 1, 'Kategori 2', 'yes', NULL, NULL),
(3, 1, 'Kategori 3', 'no', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_ecommerce`
--

CREATE TABLE `product_ecommerce` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_ecommerce` bigint(20) NOT NULL,
  `link` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_ecommerce`
--

INSERT INTO `product_ecommerce` (`id`, `id_product`, `id_ecommerce`, `link`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'https://www.tokopedia.com/', NULL, NULL),
(2, 2, 1, 'https://www.tokopedia.com/', NULL, NULL),
(3, 3, 1, 'https://www.tokopedia.com/', '2022-06-27 07:36:57', '2022-06-27 07:36:57'),
(4, 4, 1, 'https://www.tokopedia.com/', '2022-06-27 07:40:56', '2022-06-27 07:40:56'),
(5, 5, 1, 'https://www.tokopedia.com/', '2022-06-27 07:51:48', '2022-06-27 07:51:48'),
(6, 6, 1, 'https://www.tokopedia.com/', '2022-06-27 07:53:04', '2022-06-27 07:53:04'),
(7, 7, 1, 'https://www.tokopedia.com/', '2022-06-27 08:18:49', '2022-06-27 08:18:49'),
(8, 8, 1, 'https://www.tokopedia.com/', '2022-06-27 08:20:16', '2022-06-27 08:20:16'),
(9, 9, 1, 'https://www.tokopedia.com/', '2022-06-27 08:21:20', '2022-06-27 08:21:20'),
(10, 9, 1, 'https://www.tokopedia.com/', '2022-06-27 08:21:20', '2022-06-27 08:21:20'),
(11, 10, 1, 'https://www.tokopedia.com/', '2022-06-27 08:23:14', '2022-06-27 08:23:14'),
(12, 11, 1, 'https://www.tokopedia.com/', '2022-06-27 08:25:08', '2022-06-27 08:25:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_favorit`
--

CREATE TABLE `product_favorit` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `agent` text NOT NULL,
  `source` varchar(255) NOT NULL DEFAULT 'favorit',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_favorit`
--

INSERT INTO `product_favorit` (`id`, `id_product`, `id_user`, `agent`, `source`, `created_at`, `updated_at`) VALUES
(5, 1, 5, '', 'favorit', NULL, NULL),
(6, 5, 5, '', 'favorit', '2022-06-27 15:00:41', '2022-06-27 15:00:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_file`
--

CREATE TABLE `product_file` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `file` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_file`
--

INSERT INTO `product_file` (`id`, `id_product`, `file`, `created_at`, `updated_at`) VALUES
(3, 1, 'storage/upload/product/1/406877_product.png', '2022-06-24 07:21:07', '2022-06-24 07:21:07'),
(4, 1, 'storage/upload/product/1/634_product.png', '2022-06-24 07:21:07', '2022-06-24 07:21:07'),
(5, 2, 'storage/upload/product/2/394278_product.png', '2022-06-24 07:21:21', '2022-06-24 07:21:21'),
(6, 2, 'storage/upload/product/2/439299_product.png', '2022-06-24 07:21:21', '2022-06-24 07:21:21'),
(7, 3, 'storage/upload/product/3/991086_product.jpg', '2022-06-27 07:36:57', '2022-06-27 07:36:57'),
(8, 3, 'storage/upload/product/3/777487_product.jpg', '2022-06-27 07:36:57', '2022-06-27 07:36:57'),
(9, 4, 'storage/upload/product/4/52881_product.jpg', '2022-06-27 07:40:56', '2022-06-27 07:40:56'),
(10, 5, 'storage/upload/product/5/601592_product.jpg', '2022-06-27 07:51:48', '2022-06-27 07:51:48'),
(11, 6, 'storage/upload/product/6/436239_product.png', '2022-06-27 07:53:04', '2022-06-27 07:53:04'),
(12, 7, 'storage/upload/product/7/428683_product.jpg', '2022-06-27 08:18:49', '2022-06-27 08:18:49'),
(13, 8, 'storage/upload/product/8/322001_product.jpg', '2022-06-27 08:20:16', '2022-06-27 08:20:16'),
(14, 9, 'storage/upload/product/9/643215_product.jpg', '2022-06-27 08:21:20', '2022-06-27 08:21:20'),
(15, 10, 'storage/upload/product/10/179572_product.jpg', '2022-06-27 08:23:14', '2022-06-27 08:23:14'),
(16, 11, 'storage/upload/product/11/61851_product.jpg', '2022-06-27 08:25:08', '2022-06-27 08:25:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_view`
--

CREATE TABLE `product_view` (
  `id` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `agent` text NOT NULL,
  `source` varchar(255) NOT NULL DEFAULT 'view',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_view`
--

INSERT INTO `product_view` (`id`, `id_product`, `id_user`, `agent`, `source`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'PostmanRuntime/7.29.0', 'view', '2022-06-21 10:41:29', '2022-06-21 10:41:29'),
(3, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-21 10:41:45', '2022-06-21 10:41:45'),
(4, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-21 10:42:32', '2022-06-21 10:42:32'),
(5, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-21 10:42:46', '2022-06-21 10:42:46'),
(6, 1, 1, 'PostmanRuntime/7.29.0', 'view', '2022-06-21 10:43:04', '2022-06-21 10:43:04'),
(7, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-21 14:35:16', '2022-06-21 14:35:16'),
(8, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-21 14:36:08', '2022-06-21 14:36:08'),
(9, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:27:23', '2022-06-23 16:27:23'),
(10, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:32:13', '2022-06-23 16:32:13'),
(11, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:35:08', '2022-06-23 16:35:08'),
(12, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:36:14', '2022-06-23 16:36:14'),
(13, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:40:55', '2022-06-23 16:40:55'),
(14, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:43:40', '2022-06-23 16:43:40'),
(15, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:43:55', '2022-06-23 16:43:55'),
(16, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:44:47', '2022-06-23 16:44:47'),
(17, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 16:53:26', '2022-06-23 16:53:26'),
(18, 1, NULL, 'PostmanRuntime/7.28.4', 'view', '2022-06-23 16:54:31', '2022-06-23 16:54:31'),
(19, 2, NULL, 'PostmanRuntime/7.28.4', 'view', '2022-06-23 16:54:40', '2022-06-23 16:54:40'),
(20, 1, NULL, 'PostmanRuntime/7.28.4', 'view', '2022-06-23 16:54:45', '2022-06-23 16:54:45'),
(21, 1, 1, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:55:20', '2022-06-23 16:55:20'),
(22, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:55:25', '2022-06-23 16:55:25'),
(23, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:57:14', '2022-06-23 16:57:14'),
(24, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:57:16', '2022-06-23 16:57:16'),
(25, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:57:34', '2022-06-23 16:57:34'),
(26, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:57:42', '2022-06-23 16:57:42'),
(27, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:58:19', '2022-06-23 16:58:19'),
(28, 1, 5, 'PostmanRuntime/7.28.4', 'view', '2022-06-23 16:58:26', '2022-06-23 16:58:26'),
(29, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:58:33', '2022-06-23 16:58:33'),
(30, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:58:40', '2022-06-23 16:58:40'),
(31, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:58:51', '2022-06-23 16:58:51'),
(32, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:59:01', '2022-06-23 16:59:01'),
(33, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:59:07', '2022-06-23 16:59:07'),
(34, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:59:32', '2022-06-23 16:59:32'),
(35, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:59:41', '2022-06-23 16:59:41'),
(36, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:59:49', '2022-06-23 16:59:49'),
(37, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 16:59:57', '2022-06-23 16:59:57'),
(38, 1, NULL, 'PostmanRuntime/7.28.4', 'view', '2022-06-23 16:59:57', '2022-06-23 16:59:57'),
(39, 1, NULL, 'PostmanRuntime/7.28.4', 'view', '2022-06-23 17:00:12', '2022-06-23 17:00:12'),
(40, 1, NULL, 'PostmanRuntime/7.29.0', 'view', '2022-06-23 17:00:26', '2022-06-23 17:00:26'),
(41, 1, NULL, 'PostmanRuntime/7.28.4', 'view', '2022-06-23 17:00:33', '2022-06-23 17:00:33'),
(42, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 17:01:53', '2022-06-23 17:01:53'),
(43, 2, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 17:03:06', '2022-06-23 17:03:06'),
(44, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 17:04:20', '2022-06-23 17:04:20'),
(45, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 17:04:33', '2022-06-23 17:04:33'),
(46, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 17:05:07', '2022-06-23 17:05:07'),
(47, 1, NULL, 'okhttp/4.9.2', 'view', '2022-06-23 17:05:18', '2022-06-23 17:05:18'),
(48, 1, NULL, 'PostmanRuntime/7.28.4', 'view', '2022-06-24 11:23:49', '2022-06-24 11:23:49'),
(49, 1, 5, 'PostmanRuntime/7.28.4', 'view', '2022-06-24 14:23:17', '2022-06-24 14:23:17'),
(50, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 13:48:07', '2022-06-27 13:48:07'),
(51, 2, 5, 'okhttp/4.9.2', 'view', '2022-06-27 13:49:52', '2022-06-27 13:49:52'),
(52, 2, 5, 'okhttp/4.9.2', 'view', '2022-06-27 13:50:28', '2022-06-27 13:50:28'),
(53, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 13:50:35', '2022-06-27 13:50:35'),
(54, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 13:54:32', '2022-06-27 13:54:32'),
(55, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:00:19', '2022-06-27 14:00:19'),
(56, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:02:38', '2022-06-27 14:02:38'),
(57, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:03:33', '2022-06-27 14:03:33'),
(58, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:04:01', '2022-06-27 14:04:01'),
(59, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:07:21', '2022-06-27 14:07:21'),
(60, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:07:25', '2022-06-27 14:07:25'),
(61, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:08:22', '2022-06-27 14:08:22'),
(62, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:08:41', '2022-06-27 14:08:41'),
(63, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:09:04', '2022-06-27 14:09:04'),
(64, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:11:30', '2022-06-27 14:11:30'),
(65, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:12:44', '2022-06-27 14:12:44'),
(66, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:14:17', '2022-06-27 14:14:17'),
(67, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:15:21', '2022-06-27 14:15:21'),
(68, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:15:34', '2022-06-27 14:15:34'),
(69, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:15:46', '2022-06-27 14:15:46'),
(70, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:16:05', '2022-06-27 14:16:05'),
(71, 4, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:52:54', '2022-06-27 14:52:54'),
(72, 5, 5, 'okhttp/4.9.2', 'view', '2022-06-27 14:58:23', '2022-06-27 14:58:23'),
(73, 5, 5, 'okhttp/4.9.2', 'view', '2022-06-27 15:00:42', '2022-06-27 15:00:42'),
(74, 1, 5, 'okhttp/4.9.2', 'view', '2022-06-27 16:26:01', '2022-06-27 16:26:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ro_cities`
--

CREATE TABLE `ro_cities` (
  `city_id` bigint(20) NOT NULL,
  `province_id` bigint(20) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `postal_code` char(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ro_cities`
--

INSERT INTO `ro_cities` (`city_id`, `province_id`, `city_name`, `postal_code`) VALUES
(1, 21, 'Kabupaten Aceh Barat', '23681'),
(2, 21, 'Kabupaten Aceh Barat Daya', '23764'),
(3, 21, 'Kabupaten Aceh Besar', '23951'),
(4, 21, 'Kabupaten Aceh Jaya', '23654'),
(5, 21, 'Kabupaten Aceh Selatan', '23719'),
(6, 21, 'Kabupaten Aceh Singkil', '24785'),
(7, 21, 'Kabupaten Aceh Tamiang', '24476'),
(8, 21, 'Kabupaten Aceh Tengah', '24511'),
(9, 21, 'Kabupaten Aceh Tenggara', '24611'),
(10, 21, 'Kabupaten Aceh Timur', '24454'),
(11, 21, 'Kabupaten Aceh Utara', '24382'),
(12, 32, 'Kabupaten Agam', '26411'),
(13, 23, 'Kabupaten Alor', '85811'),
(14, 19, 'Kota Ambon', '97222'),
(15, 34, 'Kabupaten Asahan', '21214'),
(16, 24, 'Kabupaten Asmat', '99777'),
(17, 1, 'Kabupaten Badung', '80351'),
(18, 13, 'Kabupaten Balangan', '71611'),
(19, 15, 'Kota Balikpapan', '76111'),
(20, 21, 'Kota Banda Aceh', '23238'),
(21, 18, 'Kota Bandar Lampung', '35139'),
(22, 9, 'Kabupaten Bandung', '40311'),
(23, 9, 'Kota Bandung', '40111'),
(24, 9, 'Kabupaten Bandung Barat', '40721'),
(25, 29, 'Kabupaten Banggai', '94711'),
(26, 29, 'Kabupaten Banggai Kepulauan', '94881'),
(27, 2, 'Kabupaten Bangka', '33212'),
(28, 2, 'Kabupaten Bangka Barat', '33315'),
(29, 2, 'Kabupaten Bangka Selatan', '33719'),
(30, 2, 'Kabupaten Bangka Tengah', '33613'),
(31, 11, 'Kabupaten Bangkalan', '69118'),
(32, 1, 'Kabupaten Bangli', '80619'),
(33, 13, 'Kabupaten Banjar', '70619'),
(34, 9, 'Kota Banjar', '46311'),
(35, 13, 'Kota Banjarbaru', '70712'),
(36, 13, 'Kota Banjarmasin', '70117'),
(37, 10, 'Kabupaten Banjarnegara', '53419'),
(38, 28, 'Kabupaten Bantaeng', '92411'),
(39, 5, 'Kabupaten Bantul', '55715'),
(40, 33, 'Kabupaten Banyuasin', '30911'),
(41, 10, 'Kabupaten Banyumas', '53114'),
(42, 11, 'Kabupaten Banyuwangi', '68416'),
(43, 13, 'Kabupaten Barito Kuala', '70511'),
(44, 14, 'Kabupaten Barito Selatan', '73711'),
(45, 14, 'Kabupaten Barito Timur', '73671'),
(46, 14, 'Kabupaten Barito Utara', '73881'),
(47, 28, 'Kabupaten Barru', '90719'),
(48, 17, 'Kota Batam', '29413'),
(49, 10, 'Kabupaten Batang', '51211'),
(50, 8, 'Kabupaten Batang Hari', '36613'),
(51, 11, 'Kota Batu', '65311'),
(52, 34, 'Kabupaten Batu Bara', '21655'),
(53, 30, 'Kota Bau-Bau', '93719'),
(54, 9, 'Kabupaten Bekasi', '17837'),
(55, 9, 'Kota Bekasi', '17121'),
(56, 2, 'Kabupaten Belitung', '33419'),
(57, 2, 'Kabupaten Belitung Timur', '33519'),
(58, 23, 'Kabupaten Belu', '85711'),
(59, 21, 'Kabupaten Bener Meriah', '24581'),
(60, 26, 'Kabupaten Bengkalis', '28719'),
(61, 12, 'Kabupaten Bengkayang', '79213'),
(62, 4, 'Kota Bengkulu', '38229'),
(63, 4, 'Kabupaten Bengkulu Selatan', '38519'),
(64, 4, 'Kabupaten Bengkulu Tengah', '38319'),
(65, 4, 'Kabupaten Bengkulu Utara', '38619'),
(66, 15, 'Kabupaten Berau', '77311'),
(67, 24, 'Kabupaten Biak Numfor', '98119'),
(68, 22, 'Kabupaten Bima', '84171'),
(69, 22, 'Kota Bima', '84139'),
(70, 34, 'Kota Binjai', '20712'),
(71, 17, 'Kabupaten Bintan', '29135'),
(72, 21, 'Kabupaten Bireuen', '24219'),
(73, 31, 'Kota Bitung', '95512'),
(74, 11, 'Kabupaten Blitar', '66171'),
(75, 11, 'Kota Blitar', '66124'),
(76, 10, 'Kabupaten Blora', '58219'),
(77, 7, 'Kabupaten Boalemo', '96319'),
(78, 9, 'Kabupaten Bogor', '16911'),
(79, 9, 'Kota Bogor', '16119'),
(80, 11, 'Kabupaten Bojonegoro', '62119'),
(81, 31, 'Kabupaten Bolaang Mongondow (Bolmong)', '95755'),
(82, 31, 'Kabupaten Bolaang Mongondow Selatan', '95774'),
(83, 31, 'Kabupaten Bolaang Mongondow Timur', '95783'),
(84, 31, 'Kabupaten Bolaang Mongondow Utara', '95765'),
(85, 30, 'Kabupaten Bombana', '93771'),
(86, 11, 'Kabupaten Bondowoso', '68219'),
(87, 28, 'Kabupaten Bone', '92713'),
(88, 7, 'Kabupaten Bone Bolango', '96511'),
(89, 15, 'Kota Bontang', '75313'),
(90, 24, 'Kabupaten Boven Digoel', '99662'),
(91, 10, 'Kabupaten Boyolali', '57312'),
(92, 10, 'Kabupaten Brebes', '52212'),
(93, 32, 'Kota Bukittinggi', '26115'),
(94, 1, 'Kabupaten Buleleng', '81111'),
(95, 28, 'Kabupaten Bulukumba', '92511'),
(96, 16, 'Kabupaten Bulungan (Bulongan)', '77211'),
(97, 8, 'Kabupaten Bungo', '37216'),
(98, 29, 'Kabupaten Buol', '94564'),
(99, 19, 'Kabupaten Buru', '97371'),
(100, 19, 'Kabupaten Buru Selatan', '97351'),
(101, 30, 'Kabupaten Buton', '93754'),
(102, 30, 'Kabupaten Buton Utara', '93745'),
(103, 9, 'Kabupaten Ciamis', '46211'),
(104, 9, 'Kabupaten Cianjur', '43217'),
(105, 10, 'Kabupaten Cilacap', '53211'),
(106, 3, 'Kota Cilegon', '42417'),
(107, 9, 'Kota Cimahi', '40512'),
(108, 9, 'Kabupaten Cirebon', '45611'),
(109, 9, 'Kota Cirebon', '45116'),
(110, 34, 'Kabupaten Dairi', '22211'),
(111, 24, 'Kabupaten Deiyai (Deliyai)', '98784'),
(112, 34, 'Kabupaten Deli Serdang', '20511'),
(113, 10, 'Kabupaten Demak', '59519'),
(114, 1, 'Kota Denpasar', '80227'),
(115, 9, 'Kota Depok', '16416'),
(116, 32, 'Kabupaten Dharmasraya', '27612'),
(117, 24, 'Kabupaten Dogiyai', '98866'),
(118, 22, 'Kabupaten Dompu', '84217'),
(119, 29, 'Kabupaten Donggala', '94341'),
(120, 26, 'Kota Dumai', '28811'),
(121, 33, 'Kabupaten Empat Lawang', '31811'),
(122, 23, 'Kabupaten Ende', '86351'),
(123, 28, 'Kabupaten Enrekang', '91719'),
(124, 25, 'Kabupaten Fakfak', '98651'),
(125, 23, 'Kabupaten Flores Timur', '86213'),
(126, 9, 'Kabupaten Garut', '44126'),
(127, 21, 'Kabupaten Gayo Lues', '24653'),
(128, 1, 'Kabupaten Gianyar', '80519'),
(129, 7, 'Kabupaten Gorontalo', '96218'),
(130, 7, 'Kota Gorontalo', '96115'),
(131, 7, 'Kabupaten Gorontalo Utara', '96611'),
(132, 28, 'Kabupaten Gowa', '92111'),
(133, 11, 'Kabupaten Gresik', '61115'),
(134, 10, 'Kabupaten Grobogan', '58111'),
(135, 5, 'Kabupaten Gunung Kidul', '55812'),
(136, 14, 'Kabupaten Gunung Mas', '74511'),
(137, 34, 'Kota Gunungsitoli', '22813'),
(138, 20, 'Kabupaten Halmahera Barat', '97757'),
(139, 20, 'Kabupaten Halmahera Selatan', '97911'),
(140, 20, 'Kabupaten Halmahera Tengah', '97853'),
(141, 20, 'Kabupaten Halmahera Timur', '97862'),
(142, 20, 'Kabupaten Halmahera Utara', '97762'),
(143, 13, 'Kabupaten Hulu Sungai Selatan', '71212'),
(144, 13, 'Kabupaten Hulu Sungai Tengah', '71313'),
(145, 13, 'Kabupaten Hulu Sungai Utara', '71419'),
(146, 34, 'Kabupaten Humbang Hasundutan', '22457'),
(147, 26, 'Kabupaten Indragiri Hilir', '29212'),
(148, 26, 'Kabupaten Indragiri Hulu', '29319'),
(149, 9, 'Kabupaten Indramayu', '45214'),
(150, 24, 'Kabupaten Intan Jaya', '98771'),
(151, 6, 'Kota Jakarta Barat', '11220'),
(152, 6, 'Kota Jakarta Pusat', '10540'),
(153, 6, 'Kota Jakarta Selatan', '12230'),
(154, 6, 'Kota Jakarta Timur', '13330'),
(155, 6, 'Kota Jakarta Utara', '14140'),
(156, 8, 'Kota Jambi', '36111'),
(157, 24, 'Kabupaten Jayapura', '99352'),
(158, 24, 'Kota Jayapura', '99114'),
(159, 24, 'Kabupaten Jayawijaya', '99511'),
(160, 11, 'Kabupaten Jember', '68113'),
(161, 1, 'Kabupaten Jembrana', '82251'),
(162, 28, 'Kabupaten Jeneponto', '92319'),
(163, 10, 'Kabupaten Jepara', '59419'),
(164, 11, 'Kabupaten Jombang', '61415'),
(165, 25, 'Kabupaten Kaimana', '98671'),
(166, 26, 'Kabupaten Kampar', '28411'),
(167, 14, 'Kabupaten Kapuas', '73583'),
(168, 12, 'Kabupaten Kapuas Hulu', '78719'),
(169, 10, 'Kabupaten Karanganyar', '57718'),
(170, 1, 'Kabupaten Karangasem', '80819'),
(171, 9, 'Kabupaten Karawang', '41311'),
(172, 17, 'Kabupaten Karimun', '29611'),
(173, 34, 'Kabupaten Karo', '22119'),
(174, 14, 'Kabupaten Katingan', '74411'),
(175, 4, 'Kabupaten Kaur', '38911'),
(176, 12, 'Kabupaten Kayong Utara', '78852'),
(177, 10, 'Kabupaten Kebumen', '54319'),
(178, 11, 'Kabupaten Kediri', '64184'),
(179, 11, 'Kota Kediri', '64125'),
(180, 24, 'Kabupaten Keerom', '99461'),
(181, 10, 'Kabupaten Kendal', '51314'),
(182, 30, 'Kota Kendari', '93126'),
(183, 4, 'Kabupaten Kepahiang', '39319'),
(184, 17, 'Kabupaten Kepulauan Anambas', '29991'),
(185, 19, 'Kabupaten Kepulauan Aru', '97681'),
(186, 32, 'Kabupaten Kepulauan Mentawai', '25771'),
(187, 26, 'Kabupaten Kepulauan Meranti', '28791'),
(188, 31, 'Kabupaten Kepulauan Sangihe', '95819'),
(189, 6, 'Kabupaten Kepulauan Seribu', '14550'),
(190, 31, 'Kabupaten Kepulauan Siau Tagulandang Biaro (Sitaro)', '95862'),
(191, 20, 'Kabupaten Kepulauan Sula', '97995'),
(192, 31, 'Kabupaten Kepulauan Talaud', '95885'),
(193, 24, 'Kabupaten Kepulauan Yapen (Yapen Waropen)', '98211'),
(194, 8, 'Kabupaten Kerinci', '37167'),
(195, 12, 'Kabupaten Ketapang', '78874'),
(196, 10, 'Kabupaten Klaten', '57411'),
(197, 1, 'Kabupaten Klungkung', '80719'),
(198, 30, 'Kabupaten Kolaka', '93511'),
(199, 30, 'Kabupaten Kolaka Utara', '93911'),
(200, 30, 'Kabupaten Konawe', '93411'),
(201, 30, 'Kabupaten Konawe Selatan', '93811'),
(202, 30, 'Kabupaten Konawe Utara', '93311'),
(203, 13, 'Kabupaten Kotabaru', '72119'),
(204, 31, 'Kota Kotamobagu', '95711'),
(205, 14, 'Kabupaten Kotawaringin Barat', '74119'),
(206, 14, 'Kabupaten Kotawaringin Timur', '74364'),
(207, 26, 'Kabupaten Kuantan Singingi', '29519'),
(208, 12, 'Kabupaten Kubu Raya', '78311'),
(209, 10, 'Kabupaten Kudus', '59311'),
(210, 5, 'Kabupaten Kulon Progo', '55611'),
(211, 9, 'Kabupaten Kuningan', '45511'),
(212, 23, 'Kabupaten Kupang', '85362'),
(213, 23, 'Kota Kupang', '85119'),
(214, 15, 'Kabupaten Kutai Barat', '75711'),
(215, 15, 'Kabupaten Kutai Kartanegara', '75511'),
(216, 15, 'Kabupaten Kutai Timur', '75611'),
(217, 34, 'Kabupaten Labuhan Batu', '21412'),
(218, 34, 'Kabupaten Labuhan Batu Selatan', '21511'),
(219, 34, 'Kabupaten Labuhan Batu Utara', '21711'),
(220, 33, 'Kabupaten Lahat', '31419'),
(221, 14, 'Kabupaten Lamandau', '74611'),
(222, 11, 'Kabupaten Lamongan', '64125'),
(223, 18, 'Kabupaten Lampung Barat', '34814'),
(224, 18, 'Kabupaten Lampung Selatan', '35511'),
(225, 18, 'Kabupaten Lampung Tengah', '34212'),
(226, 18, 'Kabupaten Lampung Timur', '34319'),
(227, 18, 'Kabupaten Lampung Utara', '34516'),
(228, 12, 'Kabupaten Landak', '78319'),
(229, 34, 'Kabupaten Langkat', '20811'),
(230, 21, 'Kota Langsa', '24412'),
(231, 24, 'Kabupaten Lanny Jaya', '99531'),
(232, 3, 'Kabupaten Lebak', '42319'),
(233, 4, 'Kabupaten Lebong', '39264'),
(234, 23, 'Kabupaten Lembata', '86611'),
(235, 21, 'Kota Lhokseumawe', '24352'),
(236, 32, 'Kabupaten Lima Puluh Koto/Kota', '26671'),
(237, 17, 'Kabupaten Lingga', '29811'),
(238, 22, 'Kabupaten Lombok Barat', '83311'),
(239, 22, 'Kabupaten Lombok Tengah', '83511'),
(240, 22, 'Kabupaten Lombok Timur', '83612'),
(241, 22, 'Kabupaten Lombok Utara', '83711'),
(242, 33, 'Kota Lubuk Linggau', '31614'),
(243, 11, 'Kabupaten Lumajang', '67319'),
(244, 28, 'Kabupaten Luwu', '91994'),
(245, 28, 'Kabupaten Luwu Timur', '92981'),
(246, 28, 'Kabupaten Luwu Utara', '92911'),
(247, 11, 'Kabupaten Madiun', '63153'),
(248, 11, 'Kota Madiun', '63122'),
(249, 10, 'Kabupaten Magelang', '56519'),
(250, 10, 'Kota Magelang', '56133'),
(251, 11, 'Kabupaten Magetan', '63314'),
(252, 9, 'Kabupaten Majalengka', '45412'),
(253, 27, 'Kabupaten Majene', '91411'),
(254, 28, 'Kota Makassar', '90111'),
(255, 11, 'Kabupaten Malang', '65163'),
(256, 11, 'Kota Malang', '65112'),
(257, 16, 'Kabupaten Malinau', '77511'),
(258, 19, 'Kabupaten Maluku Barat Daya', '97451'),
(259, 19, 'Kabupaten Maluku Tengah', '97513'),
(260, 19, 'Kabupaten Maluku Tenggara', '97651'),
(261, 19, 'Kabupaten Maluku Tenggara Barat', '97465'),
(262, 27, 'Kabupaten Mamasa', '91362'),
(263, 24, 'Kabupaten Mamberamo Raya', '99381'),
(264, 24, 'Kabupaten Mamberamo Tengah', '99553'),
(265, 27, 'Kabupaten Mamuju', '91519'),
(266, 27, 'Kabupaten Mamuju Utara', '91571'),
(267, 31, 'Kota Manado', '95247'),
(268, 34, 'Kabupaten Mandailing Natal', '22916'),
(269, 23, 'Kabupaten Manggarai', '86551'),
(270, 23, 'Kabupaten Manggarai Barat', '86711'),
(271, 23, 'Kabupaten Manggarai Timur', '86811'),
(272, 25, 'Kabupaten Manokwari', '98311'),
(273, 25, 'Kabupaten Manokwari Selatan', '98355'),
(274, 24, 'Kabupaten Mappi', '99853'),
(275, 28, 'Kabupaten Maros', '90511'),
(276, 22, 'Kota Mataram', '83131'),
(277, 25, 'Kabupaten Maybrat', '98051'),
(278, 34, 'Kota Medan', '20228'),
(279, 12, 'Kabupaten Melawi', '78619'),
(280, 8, 'Kabupaten Merangin', '37319'),
(281, 24, 'Kabupaten Merauke', '99613'),
(282, 18, 'Kabupaten Mesuji', '34911'),
(283, 18, 'Kota Metro', '34111'),
(284, 24, 'Kabupaten Mimika', '99962'),
(285, 31, 'Kabupaten Minahasa', '95614'),
(286, 31, 'Kabupaten Minahasa Selatan', '95914'),
(287, 31, 'Kabupaten Minahasa Tenggara', '95995'),
(288, 31, 'Kabupaten Minahasa Utara', '95316'),
(289, 11, 'Kabupaten Mojokerto', '61382'),
(290, 11, 'Kota Mojokerto', '61316'),
(291, 29, 'Kabupaten Morowali', '94911'),
(292, 33, 'Kabupaten Muara Enim', '31315'),
(293, 8, 'Kabupaten Muaro Jambi', '36311'),
(294, 4, 'Kabupaten Muko Muko', '38715'),
(295, 30, 'Kabupaten Muna', '93611'),
(296, 14, 'Kabupaten Murung Raya', '73911'),
(297, 33, 'Kabupaten Musi Banyuasin', '30719'),
(298, 33, 'Kabupaten Musi Rawas', '31661'),
(299, 24, 'Kabupaten Nabire', '98816'),
(300, 21, 'Kabupaten Nagan Raya', '23674'),
(301, 23, 'Kabupaten Nagekeo', '86911'),
(302, 17, 'Kabupaten Natuna', '29711'),
(303, 24, 'Kabupaten Nduga', '99541'),
(304, 23, 'Kabupaten Ngada', '86413'),
(305, 11, 'Kabupaten Nganjuk', '64414'),
(306, 11, 'Kabupaten Ngawi', '63219'),
(307, 34, 'Kabupaten Nias', '22876'),
(308, 34, 'Kabupaten Nias Barat', '22895'),
(309, 34, 'Kabupaten Nias Selatan', '22865'),
(310, 34, 'Kabupaten Nias Utara', '22856'),
(311, 16, 'Kabupaten Nunukan', '77421'),
(312, 33, 'Kabupaten Ogan Ilir', '30811'),
(313, 33, 'Kabupaten Ogan Komering Ilir', '30618'),
(314, 33, 'Kabupaten Ogan Komering Ulu', '32112'),
(315, 33, 'Kabupaten Ogan Komering Ulu Selatan', '32211'),
(316, 33, 'Kabupaten Ogan Komering Ulu Timur', '32312'),
(317, 11, 'Kabupaten Pacitan', '63512'),
(318, 32, 'Kota Padang', '25112'),
(319, 34, 'Kabupaten Padang Lawas', '22763'),
(320, 34, 'Kabupaten Padang Lawas Utara', '22753'),
(321, 32, 'Kota Padang Panjang', '27122'),
(322, 32, 'Kabupaten Padang Pariaman', '25583'),
(323, 34, 'Kota Padang Sidempuan', '22727'),
(324, 33, 'Kota Pagar Alam', '31512'),
(325, 34, 'Kabupaten Pakpak Bharat', '22272'),
(326, 14, 'Kota Palangka Raya', '73112'),
(327, 33, 'Kota Palembang', '31512'),
(328, 28, 'Kota Palopo', '91911'),
(329, 29, 'Kota Palu', '94111'),
(330, 11, 'Kabupaten Pamekasan', '69319'),
(331, 3, 'Kabupaten Pandeglang', '42212'),
(332, 9, 'Kabupaten Pangandaran', '46511'),
(333, 28, 'Kabupaten Pangkajene Kepulauan', '90611'),
(334, 2, 'Kota Pangkal Pinang', '33115'),
(335, 24, 'Kabupaten Paniai', '98765'),
(336, 28, 'Kota Parepare', '91123'),
(337, 32, 'Kota Pariaman', '25511'),
(338, 29, 'Kabupaten Parigi Moutong', '94411'),
(339, 32, 'Kabupaten Pasaman', '26318'),
(340, 32, 'Kabupaten Pasaman Barat', '26511'),
(341, 15, 'Kabupaten Paser', '76211'),
(342, 11, 'Kabupaten Pasuruan', '67153'),
(343, 11, 'Kota Pasuruan', '67118'),
(344, 10, 'Kabupaten Pati', '59114'),
(345, 32, 'Kota Payakumbuh', '26213'),
(346, 25, 'Kabupaten Pegunungan Arfak', '98354'),
(347, 24, 'Kabupaten Pegunungan Bintang', '99573'),
(348, 10, 'Kabupaten Pekalongan', '51161'),
(349, 10, 'Kota Pekalongan', '51122'),
(350, 26, 'Kota Pekanbaru', '28112'),
(351, 26, 'Kabupaten Pelalawan', '28311'),
(352, 10, 'Kabupaten Pemalang', '52319'),
(353, 34, 'Kota Pematang Siantar', '21126'),
(354, 15, 'Kabupaten Penajam Paser Utara', '76311'),
(355, 18, 'Kabupaten Pesawaran', '35312'),
(356, 18, 'Kabupaten Pesisir Barat', '35974'),
(357, 32, 'Kabupaten Pesisir Selatan', '25611'),
(358, 21, 'Kabupaten Pidie', '24116'),
(359, 21, 'Kabupaten Pidie Jaya', '24186'),
(360, 28, 'Kabupaten Pinrang', '91251'),
(361, 7, 'Kabupaten Pohuwato', '96419'),
(362, 27, 'Kabupaten Polewali Mandar', '91311'),
(363, 11, 'Kabupaten Ponorogo', '63411'),
(364, 12, 'Kabupaten Pontianak', '78971'),
(365, 12, 'Kota Pontianak', '78112'),
(366, 29, 'Kabupaten Poso', '94615'),
(367, 33, 'Kota Prabumulih', '31121'),
(368, 18, 'Kabupaten Pringsewu', '35719'),
(369, 11, 'Kabupaten Probolinggo', '67282'),
(370, 11, 'Kota Probolinggo', '67215'),
(371, 14, 'Kabupaten Pulang Pisau', '74811'),
(372, 20, 'Kabupaten Pulau Morotai', '97771'),
(373, 24, 'Kabupaten Puncak', '98981'),
(374, 24, 'Kabupaten Puncak Jaya', '98979'),
(375, 10, 'Kabupaten Purbalingga', '53312'),
(376, 9, 'Kabupaten Purwakarta', '41119'),
(377, 10, 'Kabupaten Purworejo', '54111'),
(378, 25, 'Kabupaten Raja Ampat', '98489'),
(379, 4, 'Kabupaten Rejang Lebong', '39112'),
(380, 10, 'Kabupaten Rembang', '59219'),
(381, 26, 'Kabupaten Rokan Hilir', '28992'),
(382, 26, 'Kabupaten Rokan Hulu', '28511'),
(383, 23, 'Kabupaten Rote Ndao', '85982'),
(384, 21, 'Kota Sabang', '23512'),
(385, 23, 'Kabupaten Sabu Raijua', '85391'),
(386, 10, 'Kota Salatiga', '50711'),
(387, 15, 'Kota Samarinda', '75133'),
(388, 12, 'Kabupaten Sambas', '79453'),
(389, 34, 'Kabupaten Samosir', '22392'),
(390, 11, 'Kabupaten Sampang', '69219'),
(391, 12, 'Kabupaten Sanggau', '78557'),
(392, 24, 'Kabupaten Sarmi', '99373'),
(393, 8, 'Kabupaten Sarolangun', '37419'),
(394, 32, 'Kota Sawah Lunto', '27416'),
(395, 12, 'Kabupaten Sekadau', '79583'),
(396, 28, 'Kabupaten Selayar (Kepulauan Selayar)', '92812'),
(397, 4, 'Kabupaten Seluma', '38811'),
(398, 10, 'Kabupaten Semarang', '50511'),
(399, 10, 'Kota Semarang', '50135'),
(400, 19, 'Kabupaten Seram Bagian Barat', '97561'),
(401, 19, 'Kabupaten Seram Bagian Timur', '97581'),
(402, 3, 'Kabupaten Serang', '42182'),
(403, 3, 'Kota Serang', '42111'),
(404, 34, 'Kabupaten Serdang Bedagai', '20915'),
(405, 14, 'Kabupaten Seruyan', '74211'),
(406, 26, 'Kabupaten Siak', '28623'),
(407, 34, 'Kota Sibolga', '22522'),
(408, 28, 'Kabupaten Sidenreng Rappang/Rapang', '91613'),
(409, 11, 'Kabupaten Sidoarjo', '61219'),
(410, 29, 'Kabupaten Sigi', '94364'),
(411, 32, 'Kabupaten Sijunjung (Sawah Lunto Sijunjung)', '27511'),
(412, 23, 'Kabupaten Sikka', '86121'),
(413, 34, 'Kabupaten Simalungun', '21162'),
(414, 21, 'Kabupaten Simeulue', '23891'),
(415, 12, 'Kota Singkawang', '79117'),
(416, 28, 'Kabupaten Sinjai', '92615'),
(417, 12, 'Kabupaten Sintang', '78619'),
(418, 11, 'Kabupaten Situbondo', '68316'),
(419, 5, 'Kabupaten Sleman', '55513'),
(420, 32, 'Kabupaten Solok', '27365'),
(421, 32, 'Kota Solok', '27315'),
(422, 32, 'Kabupaten Solok Selatan', '27779'),
(423, 28, 'Kabupaten Soppeng', '90812'),
(424, 25, 'Kabupaten Sorong', '98431'),
(425, 25, 'Kota Sorong', '98411'),
(426, 25, 'Kabupaten Sorong Selatan', '98454'),
(427, 10, 'Kabupaten Sragen', '57211'),
(428, 9, 'Kabupaten Subang', '41215'),
(429, 21, 'Kota Subulussalam', '24882'),
(430, 9, 'Kabupaten Sukabumi', '43311'),
(431, 9, 'Kota Sukabumi', '43114'),
(432, 14, 'Kabupaten Sukamara', '74712'),
(433, 10, 'Kabupaten Sukoharjo', '57514'),
(434, 23, 'Kabupaten Sumba Barat', '87219'),
(435, 23, 'Kabupaten Sumba Barat Daya', '87453'),
(436, 23, 'Kabupaten Sumba Tengah', '87358'),
(437, 23, 'Kabupaten Sumba Timur', '87112'),
(438, 22, 'Kabupaten Sumbawa', '84315'),
(439, 22, 'Kabupaten Sumbawa Barat', '84419'),
(440, 9, 'Kabupaten Sumedang', '45326'),
(441, 11, 'Kabupaten Sumenep', '69413'),
(442, 8, 'Kota Sungaipenuh', '37113'),
(443, 24, 'Kabupaten Supiori', '98164'),
(444, 11, 'Kota Surabaya', '60119'),
(445, 10, 'Kota Surakarta (Solo)', '57113'),
(446, 13, 'Kabupaten Tabalong', '71513'),
(447, 1, 'Kabupaten Tabanan', '82119'),
(448, 28, 'Kabupaten Takalar', '92212'),
(449, 25, 'Kabupaten Tambrauw', '98475'),
(450, 16, 'Kabupaten Tana Tidung', '77611'),
(451, 28, 'Kabupaten Tana Toraja', '91819'),
(452, 13, 'Kabupaten Tanah Bumbu', '72211'),
(453, 32, 'Kabupaten Tanah Datar', '27211'),
(454, 13, 'Kabupaten Tanah Laut', '70811'),
(455, 3, 'Kabupaten Tangerang', '15914'),
(456, 3, 'Kota Tangerang', '15111'),
(457, 3, 'Kota Tangerang Selatan', '15332'),
(458, 18, 'Kabupaten Tanggamus', '35619'),
(459, 34, 'Kota Tanjung Balai', '21321'),
(460, 8, 'Kabupaten Tanjung Jabung Barat', '36513'),
(461, 8, 'Kabupaten Tanjung Jabung Timur', '36719'),
(462, 17, 'Kota Tanjung Pinang', '29111'),
(463, 34, 'Kabupaten Tapanuli Selatan', '22742'),
(464, 34, 'Kabupaten Tapanuli Tengah', '22611'),
(465, 34, 'Kabupaten Tapanuli Utara', '22414'),
(466, 13, 'Kabupaten Tapin', '71119'),
(467, 16, 'Kota Tarakan', '77114'),
(468, 9, 'Kabupaten Tasikmalaya', '46411'),
(469, 9, 'Kota Tasikmalaya', '46116'),
(470, 34, 'Kota Tebing Tinggi', '20632'),
(471, 8, 'Kabupaten Tebo', '37519'),
(472, 10, 'Kabupaten Tegal', '52419'),
(473, 10, 'Kota Tegal', '52114'),
(474, 25, 'Kabupaten Teluk Bintuni', '98551'),
(475, 25, 'Kabupaten Teluk Wondama', '98591'),
(476, 10, 'Kabupaten Temanggung', '56212'),
(477, 20, 'Kota Ternate', '97714'),
(478, 20, 'Kota Tidore Kepulauan', '97815'),
(479, 23, 'Kabupaten Timor Tengah Selatan', '85562'),
(480, 23, 'Kabupaten Timor Tengah Utara', '85612'),
(481, 34, 'Kabupaten Toba Samosir', '22316'),
(482, 29, 'Kabupaten Tojo Una-Una', '94683'),
(483, 29, 'Kabupaten Toli-Toli', '94542'),
(484, 24, 'Kabupaten Tolikara', '99411'),
(485, 31, 'Kota Tomohon', '95416'),
(486, 28, 'Kabupaten Toraja Utara', '91831'),
(487, 11, 'Kabupaten Trenggalek', '66312'),
(488, 19, 'Kota Tual', '97612'),
(489, 11, 'Kabupaten Tuban', '62319'),
(490, 18, 'Kabupaten Tulang Bawang', '34613'),
(491, 18, 'Kabupaten Tulang Bawang Barat', '34419'),
(492, 11, 'Kabupaten Tulungagung', '66212'),
(493, 28, 'Kabupaten Wajo', '90911'),
(494, 30, 'Kabupaten Wakatobi', '93791'),
(495, 24, 'Kabupaten Waropen', '98269'),
(496, 18, 'Kabupaten Way Kanan', '34711'),
(497, 10, 'Kabupaten Wonogiri', '57619'),
(498, 10, 'Kabupaten Wonosobo', '56311'),
(499, 24, 'Kabupaten Yahukimo', '99041'),
(500, 24, 'Kabupaten Yalimo', '99481'),
(501, 5, 'Kota Yogyakarta', '55222');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ro_provinces`
--

CREATE TABLE `ro_provinces` (
  `province_id` bigint(20) NOT NULL,
  `province_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ro_provinces`
--

INSERT INTO `ro_provinces` (`province_id`, `province_name`) VALUES
(1, 'Bali'),
(2, 'Bangka Belitung'),
(3, 'Banten'),
(4, 'Bengkulu'),
(5, 'DI Yogyakarta'),
(6, 'DKI Jakarta'),
(7, 'Gorontalo'),
(8, 'Jambi'),
(9, 'Jawa Barat'),
(10, 'Jawa Tengah'),
(11, 'Jawa Timur'),
(12, 'Kalimantan Barat'),
(13, 'Kalimantan Selatan'),
(14, 'Kalimantan Tengah'),
(15, 'Kalimantan Timur'),
(16, 'Kalimantan Utara'),
(17, 'Kepulauan Riau'),
(18, 'Lampung'),
(19, 'Maluku'),
(20, 'Maluku Utara'),
(21, 'Nanggroe Aceh Darussalam (NAD)'),
(22, 'Nusa Tenggara Barat (NTB)'),
(23, 'Nusa Tenggara Timur (NTT)'),
(24, 'Papua'),
(25, 'Papua Barat'),
(26, 'Riau'),
(27, 'Sulawesi Barat'),
(28, 'Sulawesi Selatan'),
(29, 'Sulawesi Tengah'),
(30, 'Sulawesi Tenggara'),
(31, 'Sulawesi Utara'),
(32, 'Sumatera Barat'),
(33, 'Sumatera Selatan'),
(34, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sharings`
--

CREATE TABLE `sharings` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `desc` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sharings`
--

INSERT INTO `sharings` (`id`, `id_admin`, `title`, `active`, `desc`, `created_at`, `updated_at`) VALUES
(1, 1, 'Cara Merawat Tanaman Hias', 'yes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Orci aliquet morbi ultricies blandit faucibus. Hac sed sociis volutpat sit donec at pellentesque. Pellentesque blandit est leo interdum sit aliquam id. At tristique amet at sit nunc, interdum. Neque libero malesuada habitasse massa massa nisi. Id elit adipiscing phasellus lacus semper ac ac congue. Vestibulum, nisl hendrerit magna tempor et, vel dapibus nisl velit. Egestas suspendisse sed massa, est eros. At adipiscing cursus eu integer. Nisi nunc pharetra, nunc sagittis dolor sit.\r\nPulvinar commodo auctor ut augue augue. Interdum dolor pulvinar suspendisse mattis sagittis lectus sagittis, sapien, feugiat. Tincidunt est mollis quam enim leo sapien. A id magna cursus dignissim nam nibh et. Lobortis at magnis dignissim egestas. Dui ante id nibh nisl, ut quisque. Vitae tellus morbi varius pretium amet. Ut magna a laoreet in nulla risus. Duis sit purus suspendisse enim.\r\nQuis eros neque ac, ut mi, cras a eget. Etiam eu lectus vestibulum vel tincidunt vitae pretium. Porta nec arcu convallis elit, aliquet maecenas sodales. Suspendisse sed amet euismod at feugiat adipiscing velit purus. Dui nulla sit venenatis sapien, ipsum risus tempor dolor. Pellentesque eget condimentum diam lectus urna faucibus tempor. Maecenas et vivamus ultricies consequat eleifend. Faucibus cras nibh.', NULL, NULL),
(2, 1, 'cara merawat bayi', 'yes', '<p>cara merawat bayi dengan benar</p>', '2022-06-27 03:58:22', '2022-06-28 07:02:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sharing_file`
--

CREATE TABLE `sharing_file` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `id_sharing` bigint(20) NOT NULL,
  `file` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sharing_file`
--

INSERT INTO `sharing_file` (`id`, `id_admin`, `id_sharing`, `file`, `created_at`, `updated_at`) VALUES
(9, 1, 1, 'storage/upload/sharing/1/86742_sharing.jpeg', '2022-06-28 04:48:13', '2022-06-28 04:48:13'),
(10, 1, 2, 'storage/upload/sharing/2/880273_sharing.jpeg', '2022-06-28 04:48:40', '2022-06-28 04:48:40'),
(11, 1, 1, 'storage/upload/sharing/1/58624_sharing.jpeg', '2022-06-28 07:14:27', '2022-06-28 07:14:27'),
(12, 1, 2, 'storage/upload/sharing/2/257783_sharing.jpeg', '2022-06-28 07:17:59', '2022-06-28 07:17:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider`
--

CREATE TABLE `slider` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `related` enum('sharing','products','event') NOT NULL,
  `id_related` bigint(20) NOT NULL,
  `file` text NOT NULL,
  `urutan` int(11) NOT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `slider`
--

INSERT INTO `slider` (`id`, `id_admin`, `related`, `id_related`, `file`, `urutan`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'sharing', 0, 'storage/upload/slider/slider.png', 1, 'yes', NULL, NULL),
(2, 1, 'sharing', 0, 'storage/upload/slider/438181_slider.jpeg', 2, 'no', '2022-06-27 02:30:56', '2022-06-27 03:13:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `store`
--

CREATE TABLE `store` (
  `id` bigint(20) NOT NULL,
  `id_province` bigint(20) NOT NULL,
  `id_city` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `whatsapp` varchar(15) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `active` enum('yes','no') NOT NULL DEFAULT 'no',
  `file` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `store`
--

INSERT INTO `store` (`id`, `id_province`, `id_city`, `name`, `email`, `password`, `whatsapp`, `remember_token`, `active`, `file`, `created_at`, `updated_at`) VALUES
(1, 9, 78, 'Tanaman Plant Shop', 'seller@store.com', '$2y$10$cJGy9InfXRezdT4.6YMQfOwNhb9xbQL1xSJhN8qmkOO1liuD0hcI.', '085891658512', 'tz8JM6Xn1pEPXLoypZVPg3LQw6wKepLgwvQ66OjWXLBbAtZ4HZBz9XqSDexe', 'yes', 'storage/upload/store/299537_store.png', NULL, '2022-06-24 07:21:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `store_activated`
--

CREATE TABLE `store_activated` (
  `id` bigint(20) NOT NULL,
  `id_store` bigint(20) DEFAULT NULL,
  `type` enum('register','activated') NOT NULL,
  `agent` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `store_followers`
--

CREATE TABLE `store_followers` (
  `id` bigint(20) NOT NULL,
  `id_store` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `store_followers`
--

INSERT INTO `store_followers` (`id`, `id_store`, `id_user`, `created_at`, `updated_at`) VALUES
(7, 1, 1, '2022-06-20 13:11:05', '2022-06-20 13:11:05'),
(11, 1, 5, '2022-06-21 15:18:29', '2022-06-21 15:18:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `store_reset_password`
--

CREATE TABLE `store_reset_password` (
  `id` bigint(20) NOT NULL,
  `id_store` bigint(20) DEFAULT NULL,
  `agent` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` enum('yes','no') NOT NULL DEFAULT 'no',
  `file` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `active`, `file`, `created_at`, `updated_at`) VALUES
(1, 'ares', 'ares@gmail.com', '$2y$10$ytietPZjM/M8HUHbrHg7kuJXrybtDkLWSN3qS8A.CPlsY6rSrMOEG', 'yes', NULL, '2022-06-17 08:23:53', '2022-06-17 08:23:53'),
(5, 'Dita Dwi ', 'ditadwi@gmail.com', '$2y$10$rWbFj5O4tdE04w0jE0.vjeiREijkCNIvUhb/li5raY5c766OcwbYC', 'yes', 'img/profile/1656401552_file.jpg', NULL, '2022-06-28 15:08:26'),
(13, 'Muhamad Ade Rohayat', 'muhamadaderohayat122@gmail.com', '$2y$10$7KwNVw.kDhQMZ2WXSUjMWOHPILSiJe1y5X/8IjErdJsA1Ylojgc3y', 'yes', 'img/profile/1656322240_6a398696d8a7726657f1de24e50c8d81.jpg', '2022-06-27 10:26:23', '2022-06-27 16:30:40'),
(14, 'Dita Cahyani', 'dita@gmail.com', '$2y$10$BffbNWwwRR4f6NxeG6jDYO8PBKQPddYK3EpB7E2b9TLj0M6nv2A52', 'no', NULL, '2022-06-28 13:30:27', '2022-06-28 13:30:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_activated`
--

CREATE TABLE `user_activated` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `type` enum('register','activated') NOT NULL,
  `agent` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_activated`
--

INSERT INTO `user_activated` (`id`, `id_user`, `type`, `agent`, `created_at`, `updated_at`) VALUES
(2, 13, 'register', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36', '2022-06-27 06:02:18', '2022-06-27 06:02:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_reset_password`
--

CREATE TABLE `user_reset_password` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `agent` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authorities_id` (`authorities_id`);

--
-- Indeks untuk tabel `admin_activity`
--
ALTER TABLE `admin_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indeks untuk tabel `authorities`
--
ALTER TABLE `authorities`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indeks untuk tabel `event_file`
--
ALTER TABLE `event_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_event` (`id_event`);

--
-- Indeks untuk tabel `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `platform_ecommerce`
--
ALTER TABLE `platform_ecommerce`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indeks untuk tabel `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_store` (`id_store`),
  ADD KEY `id_product_category` (`id_product_category`);

--
-- Indeks untuk tabel `product_buy`
--
ALTER TABLE `product_buy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indeks untuk tabel `product_ecommerce`
--
ALTER TABLE `product_ecommerce`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_ecommerce` (`id_ecommerce`);

--
-- Indeks untuk tabel `product_favorit`
--
ALTER TABLE `product_favorit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `product_file`
--
ALTER TABLE `product_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`);

--
-- Indeks untuk tabel `product_view`
--
ALTER TABLE `product_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `ro_cities`
--
ALTER TABLE `ro_cities`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `province_id` (`province_id`);

--
-- Indeks untuk tabel `ro_provinces`
--
ALTER TABLE `ro_provinces`
  ADD PRIMARY KEY (`province_id`);

--
-- Indeks untuk tabel `sharings`
--
ALTER TABLE `sharings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indeks untuk tabel `sharing_file`
--
ALTER TABLE `sharing_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_sharing` (`id_sharing`);

--
-- Indeks untuk tabel `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indeks untuk tabel `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_province` (`id_province`),
  ADD KEY `id_city` (`id_city`);

--
-- Indeks untuk tabel `store_activated`
--
ALTER TABLE `store_activated`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_store` (`id_store`);

--
-- Indeks untuk tabel `store_followers`
--
ALTER TABLE `store_followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_store` (`id_store`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `store_reset_password`
--
ALTER TABLE `store_reset_password`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_store` (`id_store`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_activated`
--
ALTER TABLE `user_activated`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `user_reset_password`
--
ALTER TABLE `user_reset_password`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `admin_activity`
--
ALTER TABLE `admin_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `authorities`
--
ALTER TABLE `authorities`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `event_file`
--
ALTER TABLE `event_file`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `features`
--
ALTER TABLE `features`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `platform_ecommerce`
--
ALTER TABLE `platform_ecommerce`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `product_buy`
--
ALTER TABLE `product_buy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `product_ecommerce`
--
ALTER TABLE `product_ecommerce`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `product_favorit`
--
ALTER TABLE `product_favorit`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `product_file`
--
ALTER TABLE `product_file`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `product_view`
--
ALTER TABLE `product_view`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT untuk tabel `sharings`
--
ALTER TABLE `sharings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `sharing_file`
--
ALTER TABLE `sharing_file`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `slider`
--
ALTER TABLE `slider`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `store`
--
ALTER TABLE `store`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `store_activated`
--
ALTER TABLE `store_activated`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `store_followers`
--
ALTER TABLE `store_followers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `store_reset_password`
--
ALTER TABLE `store_reset_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `user_activated`
--
ALTER TABLE `user_activated`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_reset_password`
--
ALTER TABLE `user_reset_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`authorities_id`) REFERENCES `authorities` (`id`);

--
-- Ketidakleluasaan untuk tabel `admin_activity`
--
ALTER TABLE `admin_activity`
  ADD CONSTRAINT `admin_activity_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `event_file`
--
ALTER TABLE `event_file`
  ADD CONSTRAINT `event_file_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`),
  ADD CONSTRAINT `event_file_ibfk_2` FOREIGN KEY (`id_event`) REFERENCES `events` (`id`);

--
-- Ketidakleluasaan untuk tabel `platform_ecommerce`
--
ALTER TABLE `platform_ecommerce`
  ADD CONSTRAINT `platform_ecommerce_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_store`) REFERENCES `store` (`id`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`id_product_category`) REFERENCES `product_category` (`id`);

--
-- Ketidakleluasaan untuk tabel `product_buy`
--
ALTER TABLE `product_buy`
  ADD CONSTRAINT `product_buy_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `product_buy_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `product_category`
--
ALTER TABLE `product_category`
  ADD CONSTRAINT `product_category_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `product_ecommerce`
--
ALTER TABLE `product_ecommerce`
  ADD CONSTRAINT `product_ecommerce_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `product_ecommerce_ibfk_2` FOREIGN KEY (`id_ecommerce`) REFERENCES `platform_ecommerce` (`id`);

--
-- Ketidakleluasaan untuk tabel `product_favorit`
--
ALTER TABLE `product_favorit`
  ADD CONSTRAINT `product_favorit_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `product_favorit_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `product_file`
--
ALTER TABLE `product_file`
  ADD CONSTRAINT `product_file_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Ketidakleluasaan untuk tabel `product_view`
--
ALTER TABLE `product_view`
  ADD CONSTRAINT `product_view_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `product_view_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `ro_cities`
--
ALTER TABLE `ro_cities`
  ADD CONSTRAINT `ro_cities_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `ro_provinces` (`province_id`);

--
-- Ketidakleluasaan untuk tabel `sharings`
--
ALTER TABLE `sharings`
  ADD CONSTRAINT `sharings_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `sharing_file`
--
ALTER TABLE `sharing_file`
  ADD CONSTRAINT `sharing_file_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`),
  ADD CONSTRAINT `sharing_file_ibfk_2` FOREIGN KEY (`id_sharing`) REFERENCES `sharings` (`id`);

--
-- Ketidakleluasaan untuk tabel `slider`
--
ALTER TABLE `slider`
  ADD CONSTRAINT `slider_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_ibfk_1` FOREIGN KEY (`id_province`) REFERENCES `ro_provinces` (`province_id`),
  ADD CONSTRAINT `store_ibfk_2` FOREIGN KEY (`id_city`) REFERENCES `ro_cities` (`city_id`);

--
-- Ketidakleluasaan untuk tabel `store_activated`
--
ALTER TABLE `store_activated`
  ADD CONSTRAINT `store_activated_ibfk_1` FOREIGN KEY (`id_store`) REFERENCES `store` (`id`);

--
-- Ketidakleluasaan untuk tabel `store_followers`
--
ALTER TABLE `store_followers`
  ADD CONSTRAINT `store_followers_ibfk_1` FOREIGN KEY (`id_store`) REFERENCES `store` (`id`),
  ADD CONSTRAINT `store_followers_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `store_reset_password`
--
ALTER TABLE `store_reset_password`
  ADD CONSTRAINT `store_reset_password_ibfk_1` FOREIGN KEY (`id_store`) REFERENCES `store` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_activated`
--
ALTER TABLE `user_activated`
  ADD CONSTRAINT `user_activated_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_reset_password`
--
ALTER TABLE `user_reset_password`
  ADD CONSTRAINT `user_reset_password_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
