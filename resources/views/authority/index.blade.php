<x-layout title="Otoritas Fitur">
  <x-breadcrumb :items="[
    ['Otoritas Fitur']
  ]" title="Otoritas Fitur" /> 
  <section class="content px-3">
    <div class="row">
      <div class="col-12 mb-3">
        <a href="{{url('authority/add')}}" class="btn btn-sm btn-primary">
          <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah
        </a>
      </div>
      <div class="col-12 mb-3">
        <x-rows.authority />
      </div>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/delete.js')}}" element="table"></script>
  </x-slot>
</x-layout>