<x-layout title="Otoritas Fitur">
  <x-breadcrumb :items="[
    ['Otoritas Fitur', url('authority')],
    ['Edit']
  ]" title="Otoritas Fitur" /> 
  @php
    $checked = $code;
  @endphp
  <section class="content px-3">
    <form class="_form" action="{{url('authority/_edit')}}" method="post">
      <x-rows.feature type="master data" title="Master Data" :checked="$checked" />
      <x-rows.feature type="single" title="Lainnya" :checked="$checked" />
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Nama Otoritas</h3>
        </div>
        <div class="card-body row">
          <input type="text" name="title" class="form-control" placeholder="Masukan nama otoritas yang ingin dibuat" value="{{$title}}" required>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <input type="hidden" name="id" value="{{$id}}" required>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </div>
    </form>
  </section>
  <x-slot name="css">
    <style>
      .custom-switch input, .custom-switch label{cursor: pointer}
    </style>
  </x-slot>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>