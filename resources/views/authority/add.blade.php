<x-layout title="Otoritas Fitur">
  <x-breadcrumb :items="[
    ['Otoritas Fitur', url('authority')],
    ['Tambah']
  ]" title="Otoritas Fitur" /> 
  <section class="content px-3">
    <form class="_form" action="{{url('authority/_create')}}" method="post">
      <x-rows.feature type="master data" title="Master Data" />
      <x-rows.feature type="single" title="Lainnya" />
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Nama Otoritas</h3>
        </div>
        <div class="card-body row">
          <input type="text" name="title" class="form-control" placeholder="Masukan nama otoritas yang ingin dibuat" required>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </div>
    </form>
  </section>
  <x-slot name="css">
    <style>
      .custom-switch input, .custom-switch label{cursor: pointer}
    </style>
  </x-slot>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>