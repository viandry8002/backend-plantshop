<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title, url('stores')],
    ['Edit']
  ]" title="{{$title}}" />
  <section class="content px-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('stores/_edit')}}" method="post">
        <div class="card-body">
          <div class="form-group col-md-12">
            <label>Province</label>
            <input type="hidden" name="id" value="{{$item->id}}" required>
          <select name="id_province" class="form-control">
            @foreach ($provinces as $p)
            <option value="{{$p->province_id}}" {{$item->id_province == $p->province_id ? 'selected' : ''}}>{{$p->province_name}}</option>
            @endforeach
          </select>
        </div>
          <div class="form-group col-md-12">
            <label>City</label>
          <select name="id_city" class="form-control">
            @foreach ($cities as $c)
            <option value="{{$c->city_id}}" {{$item->id_city == $p->city_id ? 'selected' : ''}}>{{$c->city_name}}</option>
            @endforeach
          </select>
        </div>
          <div class="form-group col-md-12">
            <label>Nama</label>
            <input type="text" name="name" class="form-control" placeholder="Masukan nama" value="{{$item->name}}" required>
          </div>
          <div class="form-group col-md-12">
            <label>Email</label>
            <input type="email" name="email" class="form-control" placeholder="Masukan email" value="{{$item->email}}" required>
          </div>
          <div class="form-group col-md-12">
            <label>Whatsapp</label>
            <input type="text" name="whatsapp" class="form-control" placeholder="Masukan whatsapp" value="{{$item->whatsapp}}" required>
          </div>
          <div class="card-footer">
            <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
            <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
          </div>
      </form>
    </div>
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit Password {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('stores/_password')}}" method="post">
        <div class="card-body row">
          <input type="hidden" name="id" value="{{$item->id}}" required>
          <div class="form-group col-md-6">
            <label for="password">Password Baru</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password Baru" required>
          </div>
          <div class="form-group col-md-6">
            <label for="password_confirmation">Konfirmasi Password Baru</label>
            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password Baru" required>
          </div>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
    <script type="text/javascript" src="{{asset('js/ckeditor5.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>