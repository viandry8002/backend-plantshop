<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title]
  ]" title="{{$title}}" />
  <section class="content px-3">
    <div class="row">
      <div class="col-md-12 col-12 mb-3">
        <x-search.store />
      </div>
      <div class="col-12 mb-3">
        <x-rows.store :collection="$collection" />
      </div>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/active.js')}}"></script>
  </x-slot>
</x-layout>