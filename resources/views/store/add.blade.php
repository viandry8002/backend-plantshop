<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title, url('users')],
    ['Tambah']
  ]" title="{{$title}}" />
  <section class="content px-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('stores/_create')}}" method="post">
        <div class="card-body">
          <div class="form-group col-md-12">
            <label>Province</label>
          <select name="id_province" class="form-control">
            @foreach ($provinces as $p)
            <option value="{{$p->province_id}}" >{{$p->province_name}}</option>
            @endforeach
          </select>
        </div>
          <div class="form-group col-md-12">
            <label>City</label>
          <select name="id_city" class="form-control">
            @foreach ($cities as $c)
            <option value="{{$c->city_id}}" >{{$c->city_name}}</option>
            @endforeach
          </select>
        </div>
          <div class="form-group col-md-12">
            <label>Nama</label>
            <input type="text" name="name" class="form-control" placeholder="Masukan nama" required>
          </div>
          <div class="form-group col-md-12">
            <label>Email</label>
            <input type="email" name="email" class="form-control" placeholder="Masukan email" required>
          </div>
          <div class="form-group col-md-12">
            <label>Whatsapp</label>
            <input type="text" name="whatsapp" class="form-control" placeholder="Masukan whatsapp" required>
          </div>
          <div class="form-group col-md-12">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Masukan password" required>
          </div>
          <div class="card-footer">
            <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
            <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
          </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>