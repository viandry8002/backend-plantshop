<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title]
  ]" title="{{$title}}" /> 
  <section class="content px-3">
    <div class="row">
      <div class="col-md-2 col-12 mb-3">
        <a href="{{url('users/add')}}" class="btn btn-primary">
          <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah
        </a>
      </div>
      <div class="col-md-10 col-12 mb-3">
        <x-search.users />
      </div>
      <div class="col-12 mb-3">
        <x-rows.users :collection="$collection" />
      </div>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/active.js')}}"></script>
  </x-slot>
</x-layout>