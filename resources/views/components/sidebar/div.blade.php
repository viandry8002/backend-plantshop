<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
  <li class="nav-item">
    <a href="{{route('dashboard')}}" class="nav-link @if(Route::currentRouteName() == 'dashboard') active @endif">
      <i class=" nav-icon fas fa-tachometer-alt"></i>
      <p>Dashboard</p>
    </a>
  </li>
  <li class="nav-item{{(in_array(Route::currentRouteName(), $master_data)) ? ' menu-open' : ''}}">
    <a href="#" class="nav-link{{(in_array(Route::currentRouteName(), $master_data)) ? ' active' : ''}}">
      <i class="nav-icon fas fa-folder"></i>
      <p>
        Master Data
        <i class="right fas fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
      <x-sidebar.menu url="{{url('sliders')}}" key="slider" :authority="$authority" />
      <x-sidebar.menu url="{{url('sharings')}}" key="sharing" :authority="$authority" />
      <x-sidebar.menu url="{{url('events')}}" key="event" :authority="$authority" />
      <x-sidebar.menu url="{{url('stores')}}" key="store" :authority="$authority" />
      <x-sidebar.menu url="{{url('productCategories')}}" key="product_category" :authority="$authority" />
      <x-sidebar.menu url="{{url('platformEcommerce')}}" key="platform_ecommerce" :authority="$authority" />
      <x-sidebar.menu url="{{url('userMobile')}}" key="user_mobile" :authority="$authority" />
    </ul>
  </li>
  <x-sidebar.menu url="{{url('users')}}" key="users" icon="fas fa-users" :authority="$authority" />
  <x-sidebar.menu url="{{url('authority')}}" key="authority" icon="fas fa-lock" :authority="$authority" />
</ul>