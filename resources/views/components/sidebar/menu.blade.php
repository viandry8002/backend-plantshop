@foreach ($feature as $e)
  @if (in_array($e->id, json_decode($authority->code, true)))
    <li class="nav-item">
      <a href="{{$url}}" class="nav-link @if(Route::currentRouteName() == $key) active @endif">
        <i class=" nav-icon {{($icon) ? $icon : 'far fa-circle'}}"></i>
        <p>{{$e->title}}</p>
      </a>
    </li>
  @endif
@endforeach