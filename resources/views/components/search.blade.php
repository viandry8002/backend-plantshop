<form method="get">
  <div class="card card-info collapsed-card">
    <div class="card-header">
      <h3 class="card-title">
        <i class="fas fa-search"></i>&nbsp;&nbsp;
        Pencarian
      </h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-plus"></i>
        </button>
      </div>
    </div>
    <div class="card-body" style="display: none">
      <div class="row">
        {{$slot}}
        <input type="hidden" name="_activity" value="search">
      </div>
    </div>
    <div class="card-footer">
      <a href="{{url($url)}}" class="btn btn-danger">Hapus Pencarian</a>
      <button type="submit" class="btn btn-info float-right">Cari Data</button>
    </div>
  </div>
</form>