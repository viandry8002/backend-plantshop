<div class="card">
  <div class="card-header">
    <h3 class="card-title">Otoritas Fitur {{$title}}</h3>
  </div>
  <div class="card-body row">
    @foreach ($items as $item)
      <div class="form-group col-md-4">
        <div class="custom-control custom-switch">
          <input type="checkbox" name="code[]" value="{{$item->id}}" class="custom-control-input" id="feature-{{$item->id}}" @if($checked) {{(in_array($item->id, $checked)) ? 'checked' : ''}} @endif>
          <label class="custom-control-label" for="feature-{{$item->id}}">{{$item->title}}</label>
        </div>
      </div>
    @endforeach
  </div>
</div>