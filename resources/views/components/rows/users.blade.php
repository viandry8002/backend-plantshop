<div class="card">
  <div class="card-body p-0 table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th style="width: 10px">No</th>
          <th>Nama</th>
          <th>Otoritas Fitur</th>
          <th>Email</th>
          <th style="width: 300px">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($collection as $key => $item)
        <tr id="data-{{$item->id}}">
          <td class="numbering">{{no($key)}}</td>
          <td>
            <code class="mr-1" style="font-size:100%">#{{$item['id']}}</code>
            <span>{{$item['name']}}</span>
          </td>
          <td>{{$item['authority']['title'] ?? ''}}</td>
          <td>{{$item['email']}}</td>
          <td id="action-{{$item->id}}">
            <a href="{{url('users/edit/'.$item['id'])}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Ubah</a>
            <form style="display: inline-block" action="{{url('users/_status')}}" data-id="{{$item->id}}" class="activeForm mt-1">
              <button type="submit" class="btn btn-{{$item['active'] == 'yes' ? 'danger' : 'success'}} btn-sm" id="active-{{$item['id']}}">
                <i class="fas fa-{{$item['active'] == 'yes' ? 'times' : 'check'}}"></i>&nbsp;&nbsp;{{$item['active'] == 'yes' ? 'Matikan' : 'Aktifkan'}}
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
{{$collection->links('vendor.pagination.simple-bootstrap-4')}}
