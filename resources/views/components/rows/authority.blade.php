<div class="card">
  <div class="card-body p-0 table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th style="width: 10px">No</th>
          <th>Nama</th>
          <th>Jumlah Akses yang dizinkan</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($items as $key => $item)
        <tr id="data-{{$item->id}}">
          <td class="numbering">{{no($key)}}</td>
          <td>{{$item->title}}</td>
          <td>{{$item->features_count}} fitur</td>
          <td id="action-{{$item->id}}">
            <a href="{{url('authority/edit/'.$item->id)}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Ubah</a>
            <form style="display: inline-block" action="{{url('authority/_delete')}}" data-id="{{$item->id}}" class="deleteForm mt-1">
              <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apa anda ingin menghapus data No.{{no($key)}}');">
                <i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
{{$items->links('vendor.pagination.simple-bootstrap-4')}}