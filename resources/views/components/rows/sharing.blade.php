<div class="card">
    <div class="card-body p-0 table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
                    <th>Title</th>
                    <th style="width: 300px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($collection as $key => $item)
                <tr id="data-{{$item->id}}">
                    <td class="numbering">{{no($key)}}</td>
                    <td>{{$item['title']}}</td>
                    <td id="action-{{$item->id}}">
                        {!! active($item->active) !!}
                        <a href="{{url('sharings/edit/'.$item['id'])}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Ubah</a>
                        <a href="{{url('sharings-file/image/'.$item['id'])}}" class="btn btn-info btn-sm"><i class="fas fa-images"></i>&nbsp;&nbsp;Image</a>
                        <form action="{{url('sharings/_delete')}}" data-id="{{$item->id}}" style="display:inline-block" class="deleteForm">
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>