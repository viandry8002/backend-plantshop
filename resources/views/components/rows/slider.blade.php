@foreach ($collection as $item)
<div class="col-sm-4" id="data-{{$item->id}}">
  <div class="card">
    <div class="card-body p-0" {!! opacity($item->active) !!}>
      <a href="{{asset($item->file)}}" target="_blank" rel="noopener noreferrer">
        <img src="{{asset($item->file)}}" class="img-fluid" style="height: 200px; width:100%;
      object-fit: cover;">
      </a>
    </div>
    <div class="card-footer">
      {!! active($item->active) !!}
      <a href="{{url('sliders/edit/'.$item->id)}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Ubah</a>
      <form action="{{url('sliders/_delete')}}" data-id="{{$item->id}}" style="display:inline-block" class="deleteForm">
        <button type="submit" class="btn btn-danger btn-sm">
          <i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus
        </button>
      </form>
    </div>
  </div>
</div>
@endforeach