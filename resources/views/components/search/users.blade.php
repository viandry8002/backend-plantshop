<x-search url="users">
  <div class="col-md-4">
    <div class="form-group">
      <label>#ID USER</label>
      <input type="text" class="form-control" name="id" placeholder="Masukan id users" value="{{$_GET['id'] ?? ''}}">
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="name" placeholder="Masukan nama users" value="{{$_GET['name'] ?? ''}}">
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Otoritas Fitur</label>
      <select class="form-control select2" name="authorities_id" style="width: 100%;">
        <option selected="selected" value="">Pilih Otoritas Fitur</option>
        @foreach ($authority as $auth)
          <option value="{{$auth->id}}"
            @if (!empty($_GET['authorities_id']) && $_GET['authorities_id'] == $auth->id)
              selected
            @endif
            >{{$auth->title}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group mb-0">
      <input type="checkbox" id="active" name="active" value="1" 
        {{(!empty($_GET['active'])) ? 'checked' : ''}}
        {{(empty($_GET['_activity'])) ? 'checked' : ''}}
      >
      <label for="active" style="user-select:none; cursor:pointer;">&nbsp;&nbsp;User yang aktif</label>
    </div>
  </div>
</x-search>