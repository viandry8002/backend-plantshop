<x-search url="sliders">
  <div class="col-md-12">
    <div class="form-group">
      <label>URL</label>
      <input type="text" class="form-control" name="url" placeholder="Masukan URL yang dicari" value="{{$_GET['url'] ?? ''}}">
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group mb-0">
      <div class="custom-control custom-switch">
        <input type="checkbox" name="active" value="yes" class="custom-control-input" id="slider_e" 
          {{!empty($_GET['active']) && $_GET['active'] == 'yes' ? 'checked' : ''}}  {{empty($_GET['_activity']) ? 'checked' : ''}}/>
        <label class="custom-control-label" for="slider_e">Slider yang ditampilkan?</label>
      </div>
    </div>
  </div>
</x-search>