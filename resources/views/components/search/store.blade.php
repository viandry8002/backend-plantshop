<x-search url="stores">
    <div class="col-md-12">
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="title" placeholder="Masukan nama yang dicari" value="{{$_GET['name'] ?? ''}}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group mb-0">
          <div class="custom-control custom-switch">
            <input type="checkbox" name="active" value="yes" class="custom-control-input" id="slider_e" 
              {{!empty($_GET['active']) && $_GET['active'] == 'yes' ? 'checked' : ''}}  {{empty($_GET['_activity']) ? 'checked' : ''}}/>
            <label class="custom-control-label" for="slider_e">Store yang ditampilkan?</label>
          </div>
        </div>
      </div>
</x-search>