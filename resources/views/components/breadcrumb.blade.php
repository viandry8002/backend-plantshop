<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>{{$title}}</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            @foreach ($items as $item)
              @if (!empty($item[1]))
                <li class="breadcrumb-item"><a href="{{$item[1]}}">{{$item[0]}}</a></li>
              @else
                <li class="breadcrumb-item active">{{$item[0]}}{{(!empty($_GET['page'])) ? ' : halaman '.$_GET['page'] : ''}}</a>
              @endif
            </li>
            @endforeach
          </ol>
        </div>
      </div>
    </div>
  </section>