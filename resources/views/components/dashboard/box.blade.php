<div class="row">
  @foreach ($boxs as $box)
  <div class="col-lg-3 col-6">
    <div class="small-box {{$box['class']}}">
      <div class="inner">
        <h3>{{$box['count']}}</h3>
        <p>{{$box['title']}}</p>
      </div>
      <div class="icon">
        <i class="{{$box['icon']}}"></i>
      </div>
      <a href="{{$box['url']}}" target="_blank" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  @endforeach
</div>
