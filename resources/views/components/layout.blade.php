<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{$title}} | {{env('APP_NAME')}}</title>
  <link rel="alternate icon" class="js-site-favicon" type="image/png" href="{{asset('img/logo/logo.png')}}">
  <link rel="icon" class="js-site-favicon" type="image/svg+xml" href="{{asset('img/logo/logo.png')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">
  <style>
    .code{color:#dc3545;}
    .matches { padding: 0px; }
    .matches > .match { margin-bottom: 10px; margin-right: 10px; display: inline-block; padding: 0.5em; background-color: #f7f7f7; border: 1px solid #ced4da; border-radius: 10px; cursor: pointer; user-select: none; }
    .collapsed-card{margin-bottom: 0px !important;}
    .author{font-size: 90%;border-bottom: 1px dashed;color: #dc3545;padding: 0px 3px;}
    .ribbon-wrapper .ribbon { font-size: 15px; text-transform: none; }
    .ribbon-wrapper { right: 7px; top: 0px; }
    .card-footer { background-color: rgb(255 255 255 / 3%); }
    .ribbon-wrapper .ribbon::before, .ribbon-wrapper .ribbon::after { display: none; }
  </style>
  @if (isset($css))
    {{ $css }}
  @endif
  @stack('css')
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <x-navbar />  

    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <a href="{{route('dashboard')}}" class="brand-link" style="text-align: center">
        {{env('APP_NAME')}}
      </a>
      <div class="sidebar">
        <nav class="mt-2">
          <x-sidebar.div />  
        </nav>
      </div>
    </aside>
    
    {{$slot}}
    <div class="pb-1"></div>
  </div>
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2022{{(date('Y') == '2022') ? '' : '-'.date('Y')}} <a href="https://wansolution.co.id/">Wanteknologi</a>.</strong> All rights reserved.
  </footer>
</div>
<script type="text/javascript" src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/all.js')}}"></script>
<script>
  const prefixURL = "{{url('')}}"
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  const features = $(`.nav .nav-treeview`).map(function(e){
    if($(this).children('li').length < 1){
      $(this).parent().remove()
    }
  });
</script>
@if (isset($js))
  {{ $js }}
@endif
@stack('js')
</body>
</html>