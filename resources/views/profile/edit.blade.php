<x-layout title="{{auth()->user()->name}}">
  <x-breadcrumb :items="[
    ['Profile']
  ]" title="Profile" /> 
  <section class="content px-3">
    <div class="card mb-5">
      <div class="card-header">
        <h3 class="card-title">Data Profile</h3>
      </div>
      <form class="_form" action="{{url('profile/_edit')}}" method="post">
        <div class="card-body row">
          <div class="form-group col-md-6">
            <label for="name">Nama</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Masukan nama" value="{{auth()->user()->name}}" required>
          </div>
          <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Masukan email" value="{{auth()->user()->email}}" required>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Ubah Password</h3>
      </div>
      <form class="_form" action="{{url('profile/_password')}}" method="post">
        <div class="card-body row">
          <div class="form-group col-md-4">
            <label for="curr_password">Password Sekarang</label>
            <input type="password" class="form-control" name="curr_password" id="curr_password" placeholder="Password Sekarang" required>
          </div>
          <div class="form-group col-md-4">
            <label for="password">Password Baru</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password Baru" required>
          </div>
          <div class="form-group col-md-4">
            <label for="password_confirmation">Konfirmasi Password Baru</label>
            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password Baru" required>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>