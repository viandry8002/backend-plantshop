<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title, url('sliders')],
    ['Tambah']
  ]" title="{{$title}}" />
  <section class="content px-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('sliders/_create')}}" method="post" enctype="multipart/form-data">
        <div class="card-body">
          <!-- <div class="form-group">
            <label>URL</label>
            <input type="url" name="url" class="form-control" placeholder="{{url('')}}">
          </div> -->
          <div class="form-group">
            <label for="file">Gambar {{$title}}</label>
            <br />
            <input type="file" name="file" id="file" accept=".jpg, .png" required>
          </div>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>