<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{env('APP_NAME')}} | Log in</title>
  <link rel="alternate icon" class="js-site-favicon" type="image/png" href="{{asset('img/logo/logo.png')}}">
  <link rel="icon" class="js-site-favicon" type="image/svg+xml" href="{{asset('img/logo/logo.png')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/jquery-validation/cmxform.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/jquery-validation/cmxformTemplate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">
  <style>
    form.cmxform label.error, label.error { display: none !important; }
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <span class="h1"><b>Backend</b>Laravel</span>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Masuk untuk memulai sesi anda</p>

      <form action="{{route('_signin')}}" method="post" id="_loginForm">
        @csrf
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block" id="_loginButton">Masuk</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/auth/signin.js')}}"></script>
</body>
</html>
