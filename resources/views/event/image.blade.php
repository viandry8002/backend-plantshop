<x-layout title="{{$title}}">
    <x-breadcrumb :items="[
      [$title, url('events')],
      ['image']
    ]" title="{{$title}}" />
    <section class="content px-3">
      <div class="row">
        <div class="col-12 mb-3">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Tambah {{$title}}</h3>
            </div>
            <form class="_form" action="{{url('events-file/_create')}}" method="post">
              <div class="card-body">
                <input type="hidden" name="id_event" value="{{$item->id}}" required>
                <div class="form-group">
                  <label for="file">Gambar {{$title}}</label>
                  <br />
                  <input type="file" name="file" id="file" accept=".jpg, .png" required>
                </div>
                <div class="card-footer">
                  <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
                  <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
                </div>
            </form>
          </div>
        </div>  
        <div class="col-12 mb-3 row">
          <x-rows.ImageEvent :collection="$collection" />
        </div>
      </div>
    </section>
    <x-slot name="js">
      <script type="text/javascript" src="{{asset('js/crud/delete.js')}}"></script>
      <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
    </x-slot>
  </x-layout>