<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title, url('events')],
    ['Tambah']
  ]" title="{{$title}}" />
  <section class="content px-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('events/_create')}}" method="post">
        <div class="card-body">
          <div class="form-group col-md-12">
            <label>Title</label>
            <input type="text" name="title" class="form-control" placeholder="Masukan title" required>
          </div>
          <div class="form-group col-md-12">
            <label>Date Start</label>
            <input type="date" name="start" id="start_date" class="form-control" required />
          </div>
          <div class="form-group col-md-12">
            <label>Date End</label>
            <input type="date" name="end" id="end_date" class="form-control" required />
          </div>
          <div class="form-group col-md-12">
            <label>Description</label>
            <textarea name="desc" placeholder="Masukan description" class="form-control ckeditor editor"></textarea>
          </div>
          <div class="card-footer">
            <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
            <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
          </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
    <script type="text/javascript" src="{{asset('js/ckeditor5.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/date-input.js')}}"></script>
  </x-slot>
</x-layout>