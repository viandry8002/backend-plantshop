<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title, url('platformEcommerce')],
    ['Tambah']
  ]" title="{{$title}}" /> 
  <section class="content px-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('platformEcommerce/_create')}}" method="post">
        <div class="card-body row">
          <div class="form-group col-md-12">
            <label>Title</label>
            <input type="text" name="title" class="form-control" placeholder="Masukan title" required>
          </div>
          <div class="form-group col-md-12">
            <label for="file">Gambar {{$title}}</label>
            <br />
            <input type="file" name="file" id="file" accept=".jpg, .png" required>
          </div>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>