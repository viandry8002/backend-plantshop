<x-layout title="Dashboard">
  <x-breadcrumb :items="[
    ['Dashboard']
  ]" title="Dashboard" />  
  <section class="content">
    <div class="container-fluid">
      <x-dashboard.box />
    </div>
  </section>
</x-layout>