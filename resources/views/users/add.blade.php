<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title, url('users')],
    ['Tambah']
  ]" title="{{$title}}" /> 
  <section class="content px-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('users/_create')}}" method="post">
        <div class="card-body row">
          <div class="form-group col-md-6">
            <label>Nama</label>
            <input type="text" name="name" class="form-control" placeholder="Masukan nama" required>
          </div>
          <div class="form-group col-md-6">
            <label>Email</label>
            <input type="email" name="email" class="form-control" placeholder="Masukan email" required>
          </div>
          <div class="form-group col-md-6">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Masukan password" required>
          </div>
          <div class="form-group col-md-6">
            <label>Otoritas Fitur</label>
            <select name="authorities_id" id="authorities_id" class="form-control" required>
              <option value="">- Pilih Otoritas -</option>
              @foreach ($authority as $auth)
                <option value="{{$auth->id}}">{{$auth->title}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>