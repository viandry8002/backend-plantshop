<x-layout title="{{$title}}">
  <x-breadcrumb :items="[
    [$title, url('users')],
    ['Edit']
  ]" title="{{$title}}" /> 
  <section class="content px-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit Profile {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('users/_edit')}}" method="post">
        <div class="card-body row">
          <div class="form-group col-md-4">
            <label>Nama</label>
            <input type="hidden" name="id" value="{{$find->id}}" required>
            <input type="text" name="name" class="form-control" placeholder="Masukan nama" value="{{$find->name}}" required>
          </div>
          <div class="form-group col-md-4">
            <label>Email</label>
            <input type="email" name="email" class="form-control" placeholder="Masukan email" value="{{$find->email}}" required>
          </div>
          <div class="form-group col-md-4">
            <label>Otoritas Fitur</label>
            <select name="authorities_id" id="authorities_id" class="form-control" required>
              <option value="">- Pilih Otoritas -</option>
              @foreach ($authority as $auth)
                <option value="{{$auth->id}}" {{$find->authorities_id == $auth->id ? 'selected' : ''}}>{{$auth->title}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit Password {{$title}}</h3>
      </div>
      <form class="_form" action="{{url('users/_password')}}" method="post">
        <div class="card-body row">
          <input type="hidden" name="id" value="{{$find->id}}" required>
          <div class="form-group col-md-6">
            <label for="password">Password Baru</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password Baru" required>
          </div>
          <div class="form-group col-md-6">
            <label for="password_confirmation">Konfirmasi Password Baru</label>
            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password Baru" required>
          </div>
        </div>
        <div class="card-footer">
          <a onclick="return history.go(-1)" class="btn btn-default" id="_backButton">Kembali</a>
          <button type="submit" class="btn btn-primary" id="_button">Simpan</button>
        </div>
      </form>
    </div>
  </section>
  <x-slot name="js">
    <script type="text/javascript" src="{{asset('js/crud/post.js')}}"></script>
  </x-slot>
</x-layout>